#ifndef DEFORMABLESURFACE_H
#define DEFORMABLESURFACE_H

#include <QDockWidget>
#include "AsrImage3D/AsrImage3D.h"

#include "AsrSurface/AsrSurface.h"
#include "GL/gl.h"


namespace Ui {
class DeformableSurface;
}

class DeformableSurface : public QDockWidget
{
    Q_OBJECT

    Asr::Seg::AsrSurface<float>* surface_{nullptr};
    Asr::Seg::AsrImage3D<float>* image_{nullptr};

    GLuint vertexBuffer_{0}, vertexElementsBuffer_{0};
//    GLrenderer* renderWidget_;
public:
    explicit DeformableSurface(QWidget *parent = 0);
    ~DeformableSurface();
//    void setRenderWidget(GLrenderer* renderWidget) { renderWdiget_ = renderWidget; }
    void setSurface(Asr::Seg::AsrSurface<float>* surface ) { surface_ = surface; }
    void setImage(Asr::Seg::AsrImage3D<float>* image ) { image_ = image; }

    void setVertexBuffers( const GLuint & vBuffer, const GLuint & vElementsBuffer) { vertexBuffer_=vBuffer; vertexElementsBuffer_=vElementsBuffer; }
public slots:
    void splitTriangles();
    int deform();
signals:

    void surfaceChanged();

private:
    Ui::DeformableSurface *ui;
};

#endif // DEFORMABLESURFACE_H
