/********************************************************************************
** Form generated from reading UI file 'segmentationexplorer.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEGMENTATIONEXPLORER_H
#define UI_SEGMENTATIONEXPLORER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SegmentationExplorer
{
public:
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *bLoadSkeleton;
    QLineEdit *lSkeletonName;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QTreeWidget *treeWidget;
    QPushButton *bCurrentCarina;
    QPushButton *bFlyFromTrachea;
    QPushButton *bSnapToTrachea;
    QLabel *label;
    QSlider *sFlyThroughSpeed;

    void setupUi(QDockWidget *SegmentationExplorer)
    {
        if (SegmentationExplorer->objectName().isEmpty())
            SegmentationExplorer->setObjectName(QStringLiteral("SegmentationExplorer"));
        SegmentationExplorer->resize(400, 305);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_2 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        bLoadSkeleton = new QPushButton(dockWidgetContents);
        bLoadSkeleton->setObjectName(QStringLiteral("bLoadSkeleton"));

        horizontalLayout->addWidget(bLoadSkeleton);

        lSkeletonName = new QLineEdit(dockWidgetContents);
        lSkeletonName->setObjectName(QStringLiteral("lSkeletonName"));

        horizontalLayout->addWidget(lSkeletonName);


        horizontalLayout_3->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(horizontalLayout_3);

        groupBox = new QGroupBox(dockWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        treeWidget = new QTreeWidget(groupBox);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setSortingEnabled(true);
        treeWidget->setColumnCount(6);

        verticalLayout->addWidget(treeWidget);

        bCurrentCarina = new QPushButton(groupBox);
        bCurrentCarina->setObjectName(QStringLiteral("bCurrentCarina"));

        verticalLayout->addWidget(bCurrentCarina);

        bFlyFromTrachea = new QPushButton(groupBox);
        bFlyFromTrachea->setObjectName(QStringLiteral("bFlyFromTrachea"));

        verticalLayout->addWidget(bFlyFromTrachea);

        bSnapToTrachea = new QPushButton(groupBox);
        bSnapToTrachea->setObjectName(QStringLiteral("bSnapToTrachea"));

        verticalLayout->addWidget(bSnapToTrachea);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        sFlyThroughSpeed = new QSlider(groupBox);
        sFlyThroughSpeed->setObjectName(QStringLiteral("sFlyThroughSpeed"));
        sFlyThroughSpeed->setMaximum(100);
        sFlyThroughSpeed->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(sFlyThroughSpeed);


        verticalLayout_2->addWidget(groupBox);

        SegmentationExplorer->setWidget(dockWidgetContents);

        retranslateUi(SegmentationExplorer);

        QMetaObject::connectSlotsByName(SegmentationExplorer);
    } // setupUi

    void retranslateUi(QDockWidget *SegmentationExplorer)
    {
        SegmentationExplorer->setWindowTitle(QApplication::translate("SegmentationExplorer", "Segmentation Explorer", 0));
        bLoadSkeleton->setText(QApplication::translate("SegmentationExplorer", "Load Seketon", 0));
        groupBox->setTitle(QApplication::translate("SegmentationExplorer", "Navigation", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(5, QApplication::translate("SegmentationExplorer", "Entry Minor Axis Length", 0));
        ___qtreewidgetitem->setText(4, QApplication::translate("SegmentationExplorer", "Entry Major Axis Length", 0));
        ___qtreewidgetitem->setText(3, QApplication::translate("SegmentationExplorer", "Carina Point", 0));
        ___qtreewidgetitem->setText(2, QApplication::translate("SegmentationExplorer", "End Point", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("SegmentationExplorer", "Generation", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("SegmentationExplorer", "Branch", 0));
        bCurrentCarina->setText(QApplication::translate("SegmentationExplorer", "Go to Current Carina Point", 0));
        bFlyFromTrachea->setText(QApplication::translate("SegmentationExplorer", "Fly From Trachea", 0));
        bSnapToTrachea->setText(QApplication::translate("SegmentationExplorer", "Snap To Trachea", 0));
        label->setText(QApplication::translate("SegmentationExplorer", "Fly Through Speed", 0));
    } // retranslateUi

};

namespace Ui {
    class SegmentationExplorer: public Ui_SegmentationExplorer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEGMENTATIONEXPLORER_H
