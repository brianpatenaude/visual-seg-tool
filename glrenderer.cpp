#include "glrenderer.h"
#include <iostream>
#include <array>

#include <GL/glu.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/transform.hpp>
#include "AsrSegStructs/AsrSegStructs.h"
#include "AsrSegStructs/AsrSegStructs_functions.hpp"

#include <chrono>
//#include <unistd.h>
#include <thread>
#include "tiffio.h"
#include <tuple>
#include <glm/ext.hpp>
using namespace std;
using namespace Asr::Seg;
using namespace std::chrono; // nanoseconds, system_clock, seconds
using namespace std::this_thread;

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

// This will identify our vertex buffer
// Generate 1 buffer, put the resulting identifier in vertexbuffer
GLrenderer::GLrenderer(QWidget *parent)
    : QGLWidget(parent) {
    //        : QOpenGLWidget(parent) {
    setFocusPolicy(Qt::StrongFocus);
}

GLrenderer::~GLrenderer(){
    glDetachShader(p_surface_, v_surface_);
    glDetachShader(p_surface_, f_surface_);

    glDeleteShader(v_surface_);    glDeleteShader(f_surface_);

    glDetachShader(p_im_texture_, v_im_texture_);
    glDetachShader(p_im_texture_, f_im_texture_);

    glDeleteShader(v_im_texture_);
    glDeleteShader(f_im_texture_);

    //    if (surface_ != nullptr) delete surface_;


}

void GLrenderer::Initialize()
{
    //connect image to surface
    deformableSurfaceWidget_->setImage(&image_);

    //(0,1,0) +z
    //  vupOrig_= glm::vec3(0,-1,0);
    //    centreOrig_  = glm::vec3(0,0,0);
    //    cameraOrig_ = glm::vec3(0,0,-10);

    //(0,1,0) - z
    vupOrig_= glm::vec3(0,1,0);
    centreOrig_  = glm::vec3(0,0,0);
    cameraOrig_ = glm::vec3(0,0,10);
}

char* GLrenderer::textFileRead(const char *fileName) {
    char* text=NULL;

    if (fileName != NULL) {
        FILE *file = fopen(fileName, "rt");

        if (file != NULL) {
            fseek(file, 0, SEEK_END);
            int count = ftell(file);
            rewind(file);

            if (count > 0) {
                text = (char*)malloc(sizeof(char) * (count + 1));
                count = fread(text, sizeof(char), count, file);
                text[count] = '\0';
            }
            fclose(file);
        }
    }
    return text;
}
void GLrenderer::takeSnapShot( const std::string & filename )
{
    cout<<"takesnapshot "<<endl;
    writeTIFF( width_, height_,filename,1);
}

int GLrenderer::writeTIFF(const unsigned int &  width, const unsigned int & height, const string & filename  , const unsigned int & compression)
{
    TIFF *file;
    GLfloat *image;
    char *image_uint,*p;

    int i;

    file = TIFFOpen(filename.c_str(), "w");
    if (file == NULL) {
        return 1;
    }
    image = (GLfloat *) malloc(width * height * sizeof(GLfloat) * 3);
    image_uint = (char *) malloc(width * height * sizeof(char) * 3);

    /* OpenGL's default 4 byte pack alignment would leave extra bytes at the
       end of each image row so that each full row contained a number of bytes
       divisible by 4.  Ie, an RGB row with 3 pixels and 8-bit componets would
       be laid out like "RGBRGBRGBxxx" where the last three "xxx" bytes exist
       just to pad the row out to 12 bytes (12 is divisible by 4). To make sure
       the rows are packed as tight as possible (no row padding), set the pack
       alignment to 1. */
    glPixelStorei(GL_PACK_ALIGNMENT, 1);

    glReadPixels(0, 0, width, height, GL_RGB, GL_FLOAT, image);
    GLfloat* im_fptr = image;
    char* im_uptr = image_uint;
    for (int i = 0 ; i < width * height*3; ++i,++im_fptr, ++im_uptr)
    {
        *im_uptr = static_cast<unsigned char>(((*im_fptr)*255));
        //        cout<<"im_uptr "<<((*im_fptr)*255)<<" "<<(*im_fptr)<<" "<<static_cast<unsigned char>(((*im_fptr)*255))<<endl;
    }

    TIFFSetField(file, TIFFTAG_IMAGEWIDTH, (uint32) width);
    TIFFSetField(file, TIFFTAG_IMAGELENGTH, (uint32) height);
    //    TIFFSetField(file, TIFFTAG_BITSPERSAMPLE, sizeof(GL_FLOAT)*8);
    TIFFSetField(file, TIFFTAG_BITSPERSAMPLE, 8);

    TIFFSetField(file, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);

    //    TIFFSetField(file, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
    //TIFFSetField(file, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
    TIFFSetField(file, TIFFTAG_COMPRESSION, compression);
    TIFFSetField(file, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
    TIFFSetField(file, TIFFTAG_SAMPLESPERPIXEL, 3);
    TIFFSetField(file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(file, TIFFTAG_ROWSPERSTRIP, 1);
    TIFFSetField(file, TIFFTAG_IMAGEDESCRIPTION, "OpenGL-rendered using brview");
    p = image_uint;
    for (i = height - 1; i >= 0; i--) {
        if (TIFFWriteScanline(file, p, i, 0) < 0) {
            free(image);
            TIFFClose(file);
            return 1;
        }
        p += width*3;// * sizeof(GLfloat) * 3;
    }
    TIFFClose(file);
    free(image);
    free(image_uint);

    return 0;
}

void GLrenderer::checkCompileShader( const GLuint & shader){


    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        cout<<"failed to compile"<<endl;
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

        // Provide the infolog in whatever manor you deem best.
        // Exit with failure.
        glDeleteShader(shader); // Don't leak the shader.
        return;
    }
    cout<<"Successfully compiled shader : "<<shader<<endl;
    // Shader compilation is successful.
}
void GLrenderer::setDeformableSurfaceWidget( DeformableSurface *deformableSurfaceWidget )
{
    deformableSurfaceWidget_ =deformableSurfaceWidget;
    deformableSurfaceWidget_->setSurface(&surface_);
    connect(deformableSurfaceWidget_,SIGNAL(surfaceChanged()),this,SLOT(update()));
}

void GLrenderer::setSegmentationExplorerWidget( SegmentationExplorer* segmentationExplorerWidget ){
    m_SegmentationExplorerWidget = segmentationExplorerWidget;
    // connect(m_SegmentationExplorerWidget, SIGNAL(flyThroughTrachea()),this, SLOT(flyThroughTrachea()));
}



void GLrenderer::setShaders() {

    cout<<"setShaders"<<endl;
    //---------------------set shader for surfaces------------------------------//
    char *vs_surface, *fs_surface;
    //    // Create shader handlers

    v_surface_ = glCreateShader(GL_VERTEX_SHADER);
    //    g = glCreateShader(GL_GEOMETRY_SHADER);
    f_surface_= glCreateShader(GL_FRAGMENT_SHADER);

    //    // Read source code from files
    vs_surface = textFileRead((QApplication::applicationDirPath() + "/glsl_shaders/basic_shader.vert").toUtf8().data());
    //    gs = textFileRead("example.geom");
    fs_surface = textFileRead((QApplication::applicationDirPath() + "/glsl_shaders/basic_shader.frag").toUtf8().data());

    const char * vv = vs_surface;
    //    const char * gg = gs;
    const char * ff = fs_surface;

    //    // Set shader source
    glShaderSource(v_surface_, 1, &vv,NULL);
    glShaderSource(f_surface_, 1, &ff,NULL);

    //    free(vs);free(gs);free(fs);
    if(vs_surface != NULL)  free(vs_surface);
    if (fs_surface != NULL) free(fs_surface);


    //    // Compile all shaders
    glCompileShader(v_surface_ );
    checkCompileShader(v_surface_);


    //    glCompileShader(g);
    glCompileShader(f_surface_);
    checkCompileShader(f_surface_);

    //    // Create the program
    p_surface_= glCreateProgram();

    // Attach shaders to program
    glAttachShader(p_surface_,v_surface_);
    //    glAttachShader(p,g);
    glAttachShader(p_surface_,f_surface_);

    //    // Link and set program to use
    glLinkProgram(p_surface_);
    glUseProgram(p_surface_);
    surfaceInVertexLoc_ = glGetAttribLocation(p_surface_, "InVertex");
    surfaceInNormalLoc_ = glGetAttribLocation(p_surface_, "InNormal");
    surfaceInScalarLoc_ = glGetAttribLocation(p_surface_, "InScalar");
    Normal_id_surface_=glGetUniformLocation(p_surface_, "NormalMatrix");
    viewModel_id_surface_=glGetUniformLocation(p_surface_,"viewModelMatrix");
    MVPid_surface_=glGetUniformLocation(p_surface_, "MVP");
    l_dir_id_=glGetUniformLocation(p_surface_, "l_dir");

    cout<<"Using shader program "<<p_surface_<<" INvertexLoc "<<surfaceInVertexLoc_<<" "<<surfaceInNormalLoc_<<" "<<surfaceInScalarLoc_<<endl;
    cout<<"normalmatrix location "<<Normal_id_surface_<<" "<< viewModel_id_surface_<<" "<<l_dir_id_<<endl;
    //**********************Texture Image Shader********************//

    char* vs_im_texture,*fs_im_texture;
    vs_im_texture      = textFileRead((QApplication::applicationDirPath() + "/glsl_shaders/image_texture.vert").toUtf8().data());
    fs_im_texture= textFileRead((QApplication::applicationDirPath() + "/glsl_shaders/image_texture.frag").toUtf8().data());
    cout<<"done reading shaders fo rim texture"<<endl;
    v_im_texture_ = glCreateShader(GL_VERTEX_SHADER);
    f_im_texture_ = glCreateShader(GL_FRAGMENT_SHADER);
    const char * vv_im_texture = vs_im_texture;
    const char * ff_im_texture = fs_im_texture;
    glShaderSource(v_im_texture_, 1, &vv_im_texture,NULL);
    glShaderSource(f_im_texture_, 1, &ff_im_texture,NULL);

    if (vs_im_texture != NULL) free(vs_im_texture);
    if (fs_im_texture != NULL) free(fs_im_texture);

    glCompileShader(v_im_texture_);
    checkCompileShader(v_im_texture_);

    glCompileShader(f_im_texture_);
    checkCompileShader(f_im_texture_);

    p_im_texture_ = glCreateProgram();
    //v_surf_glsl_programs.push_back(p_im_texture);

    glAttachShader(p_im_texture_,v_im_texture_);
    glAttachShader(p_im_texture_,f_im_texture_);

    glLinkProgram(p_im_texture_);
    glUseProgram(p_im_texture_);
    imageTexCoordLoc_ = glGetAttribLocation(p_im_texture_, "InTexCoord");
    imageInVertexLoc_ = glGetAttribLocation(p_im_texture_, "InVertex");

    //get id
    MVPid_image_=glGetUniformLocation(p_im_texture_, "MVP");
    VOX2MMid_ = glGetUniformLocation(p_im_texture_, "VOX2WORLD");
    m_imageLevelID = glGetUniformLocation(p_im_texture_, "level");
    m_imageWindowID = glGetUniformLocation(p_im_texture_, "window");
    m_imageOpacityThreshID = glGetUniformLocation(p_im_texture_, "opacityThresh");
    cout<<"window and level id "<<m_imageLevelID<<" "<<m_imageWindowID<<endl;
    VOX2MM_ = glm::mat4(1.0);
    glUniformMatrix4fv(VOX2MMid_, 1, GL_FALSE, &VOX2MM_[0][0]);

    cout<<"Using shader program for texture "<<p_im_texture_<<" textInVertex " <<imageInVertexLoc_<<" texCoordLoc "<<imageTexCoordLoc_<<" "<<VOX2MMid_<<endl;

    //----------------------------------------------------------------//





}



void GLrenderer::setUpFrameRenderBuffers(GLuint* fbo , GLuint *rbo )
{
    glGenFramebuffers(1,fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, *fbo);

    glGenRenderbuffers(NumRenderbuffers, rbo);
    glBindRenderbuffer(GL_RENDERBUFFER,rbo[Color]);

    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,width_,height_);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER ,rbo[Color]);
    glBindRenderbuffer(GL_RENDERBUFFER,rbo[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,width_,height_);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER ,rbo[Depth]);


}

void GLrenderer::initializeGL()
{

    //----------check openGL versions--------------//
    cout<<"initialize GL "<<endl;
    if (QGLFormat::OpenGL_Version_3_0)
        cout<<"openGL 3.0 is supported"<<endl;
    else if (QGLFormat::OpenGL_Version_2_1)
        cout<<"openGL 2.1 is supported"<<endl;
    else if (QGLFormat::OpenGL_Version_2_0)
        cout<<"openGL 2.0 is supported"<<endl;
    else
    {
        cerr<<"Software has not been tested below openGL 3.0"<<endl;
        exit (EXIT_FAILURE);
    }
//    glClearColor(0.1f,0.1f,0.1f,1.0f);
    glClearColor(0.1f,0.1f,0.1f,1.0f);

    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 50.0 };
    GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };


    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);



    GLint maxTextureSize{0}, max3DTextureSize{0};

    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);

    glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &max3DTextureSize);

    cout<<"Maximum texture size "<<maxTextureSize<<endl;
    cout<<"Maximum 3D texture size "<<max3DTextureSize<<endl;

//setUpFrameRenderBuffers(fboAxial_,rboAxial_);



    ///Generate image textures
    ///
    //    glUseProgram(p_im_texture_);
    glActiveTexture(GL_TEXTURE0);
    glEnable ( GL_TEXTURE_3D ) ;

    glActiveTexture(GL_TEXTURE0);//+number_of_images);
    glGenTextures(1, &texName_);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, texName_);
    texLoc_= glGetUniformLocation(p_im_texture_, "tex0");
    //    glUniform1i(texLoc_, 0);



    // Generate 1 buffer, put the resulting identifier in vertexbuffer
    glGenBuffers(1, &vertexBuffer_);
    glGenBuffers(1, &vertexElementsBuffer_);
    glGenBuffers(1, &vertexBufferCamera_);
    glGenBuffers(1, &vertexElementsBufferCamera_);
    glGenBuffers(1, &vertexBufferImage_);
    glGenBuffers(1, &vertexElementsBufferImage_);
    deformableSurfaceWidget_->setVertexBuffers(vertexBuffer_,vertexElementsBuffer_);


    width_=size().rwidth();
    height_=size().rheight();
    cout<<"width/height : "<<width_<<"/"<<height_<<endl;
    setUpFrameRenderBuffers(&fbo_,rbo_);
    setUpFrameRenderBuffers(&fboAxial_,rboAxial_);
    setUpFrameRenderBuffers(&fboCor_,rboCor_);
    setUpFrameRenderBuffers(&fboSag_,rboSag_);
    setUpFrameRenderBuffers(&fboOblique_,rboOblique_);


//    glBindFramebuffer(GL_FRAMEBUFFER, fboAxial_);
//    glBindRenderbuffer(GL_RENDERBUFFER,rboAxial_[Color]);
    cout<<"Render buffer objects "<<rbo_[Color]<<" "<<rbo_[Depth]<<endl;
    cout<<"Render buffer objects "<<rboAxial_[Color]<<" "<<rboAxial_[Depth]<<endl;
    cout<<"Render buffer objects "<<rboSag_[Color]<<" "<<rboSag_[Depth]<<endl;

cout<<"framebuffer objects "<<fbo_<<" "<<fboAxial_<<" "<<fboSag_<<endl;
//    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,width_,height_);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER ,rbo_[Color]);
//    glBindRenderbuffer(GL_RENDERBUFFER,rbo_[Depth]);
//    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,width_,height_);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER ,rbo_[Depth]);



    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if  (status!=GL_FRAMEBUFFER_COMPLETE)
    {
        cerr<<"Incomplete Framebuffer Object"<<endl;
        exit (EXIT_FAILURE);
    }

    if  (status!=GL_FRAMEBUFFER_COMPLETE)
    {
        cerr<<"Incomplete Framebuffer Object1"<<endl;
        const char *err_str = 0;
        char buf[80];
        //cout<<"status "<<status<<endl;
        switch ( status )
        {
        case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
            err_str = "UNSUPPORTED";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
            err_str = "INCOMPLETE ATTACHMENT";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
            err_str = "INCOMPLETE DRAW BUFFER";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
            err_str = "INCOMPLETE READ BUFFER";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
            err_str = "INCOMPLETE MISSING ATTACHMENT";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT:
            err_str = "INCOMPLETE MULTISAMPLE";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT:
            err_str = "INCOMPLETE LAYER TARGETS";
            break;

            // Removed in version #117 of the EXT_framebuffer_object spec
            //case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT:

        default:
            sprintf( buf, "0x%x", status );
            err_str = buf ;
            break;
        }
        //  exit (EXIT_FAILURE);
    }else{

        cout<<"Frame Buffer Object : "<<fbo_<<endl;
    }


//    glBindFramebuffer(GL_FRAMEBUFFER, fboAxial_)
    setShaders();
    //setup camera

    mView_ = glm::mat4(1.0);
    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );
    //    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    mProjection_ = glm::perspective(glm::radians(45.0f), (float) width_ / (float)height_, 0.1f, 10000.0f);


    // Model matrix : an identity matrix (model will be at the origin)
    mModel_ = glm::mat4(1.0f);

    setMVP();

    cout<<"MVP ids "<<MVPid_surface_<<" "<<MVPid_image_<<endl;


    glEnable(GL_TEXTURE_3D);


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0f);
    glDepthMask(GL_TRUE);

    //   glEnable(GL_CULL_FACE);
    //  glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);


    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


    //   glEnable(GL_TEXTURE_3D);


    //    glDisable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0f);
    glDepthMask(GL_TRUE);

    //   glEnable(GL_CULL_FACE);
    //  glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);


    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    glAlphaFunc ( GL_GREATER, 0.01 ) ;
    glEnable ( GL_ALPHA_TEST ) ;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );
    //glFrontFace(GL_CW);
    glFrontFace(GL_CCW);

    glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
    //    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    update();
}
void GLrenderer::moveDown()
{
    cameraOrig_ -= vup_;
    centreOrig_ -= vup_;
//    cameraOrig_.z -= 1;
//    centreOrig_.z -= 1;
    setMVP();
    update();

}

void GLrenderer::moveUp(){
cameraOrig_ += vup_;
centreOrig_ += vup_;

    //    cameraOrig_.z += 1;
//    centreOrig_.z += 1;
    setMVP();
    update();

}
void GLrenderer::moveLeft(){
//    cameraOrig_.x -= 1;
//    centreOrig_.x -= 1;
    glm::vec3 vlook = centreOrig_ - cameraOrig_;
    glm::vec3 vright = glm::cross(vlook, vup_);
    glm::normalize(vright);
    cameraOrig_-= vright;
    centreOrig_ -= vright;
    setMVP();
    update();

}
void GLrenderer::moveRight(){
    glm::vec3 vlook = centreOrig_ - cameraOrig_;
    glm::vec3 vright = glm::cross(vlook, vup_);
    glm::normalize(vright);
    cameraOrig_+= vright;
    centreOrig_ += vright;

//    cameraOrig_.x += 1;
//    centreOrig_.x += 1;
    setMVP();
    update();

}
void GLrenderer::moveForward(){
    cout<<"moveF"<<endl;
    glm::vec3 vlook = centreOrig_ - cameraOrig_;
//    glm::vec3 vright = glm::cross(vlook, vup_);
    glm::normalize(vlook);
    cameraOrig_+= vlook;
    centreOrig_+= vlook;

//    glm::vec3 look = centreOrig_ - cameraOrig_;
//    glm::normalize(look);
//    cameraOrig_ += look;
    setMVP();
    update();
}

void GLrenderer::moveBackward(){
    glm::vec3 look = centreOrig_ - cameraOrig_;
    glm::normalize(look);
    cameraOrig_ -= look;
    centreOrig_ -= look;

    setMVP();
    update();
}

void GLrenderer::setCameraCentre( const float & x, const float & y , const float & z ){
    cameraOrig_.x=x;
    cameraOrig_.y=y;
    cameraOrig_.z=z;

    mView_[3][0]=z;
    mView_[3][1]=y;
    mView_[3][2]=x;

    updateCameraOrientaton();

//    cout<<"setxyz "<<x<<" "<<y<<" "<<z<<endl;
//    setMVP();
//    update();

}

void GLrenderer::updateCameraOrientaton( ){

    float a{qOrientation_.a};
    float b{qOrientation_.b};
    float c{qOrientation_.c};
    float d{qOrientation_.d};

    array<float, 9> R;
    R[0] = a * a + b * b - c * c - d * d;
    R[1] = 2 * b * c - 2 * a * d;
    R[2] = 2 * b * d + 2 * a * c;
    R[3] = 2 * b * c + 2 * a * d;
    R[4] = a * a + c * c - b * b - d * d;
    R[5] = 2 * c * d - 2 * a * b;
    R[6] = 2 * b * d - 2 * a * c;
    R[7] = 2 * c * d + 2 * a * b;
    R[8] = a * a + d * d - c * c - b * b;

    cout<<R[0]<<" "<<R[1]<<" "<<R[2]<<endl;
    cout<<R[3]<<" "<<R[4]<<" "<<R[5]<<endl;
    cout<<R[6]<<" "<<R[7]<<" "<<R[8]<<endl;
    float zfac=1;
    centreOrig_.x = cameraOrig_.x + zfac*R[2];
    centreOrig_.y = cameraOrig_.y + zfac*R[5];
    centreOrig_.z = cameraOrig_.z + zfac*R[8];

//assume (1,1,0) look
    //up is x
    //quaternion * (0,-1,0)
    float yfac = 1;
    vupOrig_.x = yfac* R[1];//flip RL
    vupOrig_.y = yfac* R[4];//flip AP
    vupOrig_.z = yfac*R[7];
//    float yfac = 1;
//    vupOrig_.x = yfac* R[0];//flip RL
//    vupOrig_.y = yfac* R[3];//flip AP
//    vupOrig_.z = yfac*R[6];

    setMVP();
    update();

}

void GLrenderer::setLookVector( const float & a, const float & b, const float & c , const float & d ){

    qOrientation_= quaternion<float>(sqrt(1-b*b-c*c-d*d),b,c,d);
    updateCameraOrientaton();


}
void GLrenderer::setToFlyThrough(){

    int flyThroughSpeed = m_SegmentationExplorerWidget->GetFlyThroughSpeed();
    //    recentreView(glm::vem_SegmentationExplorerWidgetc3(100.0,0.0,0.0),glm::vec3(100.0,1.0,1.0));
    AsrContour<float> fly_path = m_SegmentationExplorerWidget->GetFlyThroughPath();
    cout<<"fly through trachea1 "<<fly_path.size()<<endl;
    vec3<float> origin = image_.GetOrigin();
    cout<<"origin "<<origin.to_string()<<endl;
    AsrContour<float> slice_path = fly_path;

    slice_path.translate(vec3<float>(abs(origin.x), abs(origin.y),abs(origin.z)));

    if (fly_path.size()>1){

        //        done in mainwindow
        //        setElevation( 0 );
        //        setAzimuth( 0 );
        auto camOrigSlice = slice_path.cbegin();

        auto camOrig = fly_path.cbegin();
        //        auto nextCamOrig = fly_path.cbegin();

        auto centreOrig = fly_path.cbegin();
        advance(centreOrig,1);

        auto nextCentreOrig = fly_path.cbegin();
        advance(nextCentreOrig,2);


        auto i_end = fly_path.cend();
        i_end--;
        //        i_end--;

        for ( ; centreOrig != i_end; centreOrig++,nextCentreOrig++, camOrig++,camOrigSlice++ )
        {
            cameraOrig_=glm::vec3(camOrig->x,camOrig->y,camOrig->z);
            centreOrig_= glm::vec3(centreOrig->x,centreOrig->y,centreOrig->z);
            //            cout<<"startcam "<<cameraOrig_.x<<" "<<cameraOrig_.y<<" "<<cameraOrig_.z<<endl;
            //            cout<<"startcent "<<centreOrig_.x<<" "<<centreOrig_.y<<" "<<centreOrig_.z<<endl;
            int Nsteps{flyThroughSpeed};
            //supersample to slow down
            vec3<float> dxyz = vec3<float>(centreOrig->x - camOrig->x,centreOrig->y - camOrig->y, centreOrig->z - camOrig->z) ;
            vec3<float> dcxyz = vec3<float>(nextCentreOrig->x - centreOrig->x,nextCentreOrig->y - centreOrig->y, nextCentreOrig->z - centreOrig->z) ;

            //            vec3<float> dxyz =*centreOrig  - *camOrig;
            dxyz/=Nsteps;
            dcxyz/=Nsteps;


            //assumes linear
            glm::vec3 ldir = glm::normalize( glm::vec3(centreOrig->x - camOrig->x,centreOrig->y - camOrig->y,centreOrig->z - camOrig->z ) );
            glUseProgram(p_surface_);
            glUniform3fv(l_dir_id_, 1, &ldir[0]);

            for (unsigned int i = 0; i < Nsteps; ++i){
                 cout<<"stepping "<<i<<" "<<cameraOrig_.x<<" "<<cameraOrig_.y<<" "<<cameraOrig_.z<<endl;
                //                cout<<"stepping "<<i<<" "<<centreOrig_.x<<" "<<centreOrig_.y<<" "<<centreOrig_.z<<endl;
                cameraOrig_.x += dxyz.x;
                cameraOrig_.y += dxyz.y;
                cameraOrig_.z += dxyz.z;

                centreOrig_.x += dcxyz.x;
                centreOrig_.y += dcxyz.y;
                centreOrig_.z += dcxyz.z;


                setMVP();
                imageSlices_[0] = camOrigSlice->x / image_.xdim();
                imageSlices_[1] = camOrigSlice->y / image_.ydim();
                imageSlices_[2] = camOrigSlice->z / image_.zdim();
                cout<<"imageSlcies "<<imageSlices_[0]<<" "<<imageSlices_[1]<<" "<<imageSlices_[2]<<endl;
                generateSlicePlanes();
                repaint();
//                update();
//                QCoreApplication::processEvents();
                //                update();
                //                updateGL();
                //                emit update();
                sleep_for(microseconds(1000));

            }
        }
        cout<<"Set cam "<<cameraOrig_.x<<" "<<cameraOrig_.y<<" "<<cameraOrig_.z<<endl;
        //visualize last step
        imageSlices_[0] = camOrigSlice->x / image_.xdim();
        imageSlices_[1] = camOrigSlice->y / image_.ydim();
        imageSlices_[2] = camOrigSlice->z / image_.zdim();
        cout<<"imageSlcies "<<imageSlices_[0]<<" "<<imageSlices_[1]<<" "<<imageSlices_[2]<<endl;
        generateSlicePlanes();
        cameraOrig_=glm::vec3(camOrig->x,camOrig->y,camOrig->z);
        centreOrig_= glm::vec3(centreOrig->x,centreOrig->y,centreOrig->z);
        glUseProgram(p_surface_);
        glm::vec3 ldir = glm::normalize( glm::vec3(centreOrig->x - camOrig->x,centreOrig->y - camOrig->y,centreOrig->z - camOrig->z ) );
        glUniform3fv(l_dir_id_, 1, &ldir[0]);
        setMVP();


        update();
    }

}



void GLrenderer::flyThroughTrachea(){
    cout<<"fly through trachea1"<<endl;
    //    recentreView(glm::vec3(100.0,0.0,0.0),glm::vec3(100.0,1.0,1.0));
    cameraOrig_=glm::vec3(90.34,-199.7,-66.6);
    centreOrig_= glm::vec3(90.34,-186.7,-78.2);

    glUseProgram(p_surface_);
    glm::vec3 ldir = glm::normalize( glm::vec3(0.0,13.0,-11.6) );
    glUniform3fv(l_dir_id_, 1, &ldir[0]);


    setMVP();
    update();
}

void GLrenderer::zoomIn(){
    //cout<<"start zoom "<<endl;
    // glm::zoomVec = centre_-camera_;zoomSensitivity_
    cameraOrig_ += (centreOrig_-cameraOrig_) * zoomSensitivity_;
//    centreOrig_ += (centreOrig_-cameraOrig_) * zoomSensitivity_;

    //mView_[3][0] = cameraOrig_.x;
    //mView_[3][1] = cameraOrig_.y;
    //mView_[3][2] = cameraOrig_.z;

    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );

    // Model matrix : an identity matrix (model will be at the origin)
    // mModel_ = glm::mat4(1.0f);
    // Our ModelViewProjection : multiplication of our 3 matrices
    //    MVP_ = mProjection_ * mView_ * mModel_; // Remember, matrix multiplication is the other way around
    setMVP();
    //    glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP_[0][0]);
    //    cout<<"update "<<endl;

    update();
}
void GLrenderer::zoomOut(){// glm::zoomVec = centre_-camera_;zoomSensitivity_
    cameraOrig_ -= (centreOrig_-cameraOrig_) * zoomSensitivity_;
//    centreOrig_ -= (centreOrig_-cameraOrig_) * zoomSensitivity_;

    //    mView_[3][0] = cameraOrig_.x;
    //    mView_[3][1] = cameraOrig_.y;
    //    mView_[3][2] = cameraOrig_.z;

    ////    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );

    //    // Model matrix : an identity matrix (model will be at the origin)
    //    // mModel_ = glm::mat4(1.0f);
    //    // Our ModelViewProjection : multiplication of our 3 matrices
    //    MVP_ = mProjection_ * mView_ * mModel_; // Remember, matrix multiplication is the other way around
    setMVP();
    //    glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP_[0][0]);

    update();

}


void GLrenderer::setElevation( const float & angle)
{// glm::zoomVec = centre_-camera_;zoomSensitivity_


    float angle_rads = glm::radians(angle);
    //    camera_ -= centre_;
    //camera_ = glm::vec3(glm::rotate( angle_rads - elevationAngle_, glm::vec3(0,1,0)) * glm::vec4(camera_,1.0)) + centre_;
    //    camera_ =  glm::vec3(glm::rotate( azimuthAngle_, glm::vec3(0,0,1)) * glm::rotate( angle_rads, glm::vec3(0,1,0))
    //                                      * glm::vec4(cameraOrig_,1.0));
    //    + centre_;

    elevationAngle_ = angle_rads;

    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );

    //    // Our ModelViewProjection : multiplication of our 3 matrices
    //    MVP_ = mProjection_ * mView_ * mModel_; // Remember, matrix multiplication is the other way around
    setMVP();
    //    glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP_[0][0]);

    update();

}


void GLrenderer::setAzimuth( const float & angle)
{
    float angle_rads = glm::radians(angle);

    //    camera_ -= centre_;
    // camera_ = glm::vec3(glm::rotate( angle_rads - azimuthAngle_, glm::vec3(0,0,1)) * glm::vec4(camera_,1.0)) + centre_;
    //    camera_ = glm::vec3(glm::rotate( angle_rads, glm::vec3(0,0,1)) * glm::rotate( elevationAngle_, glm::vec3(0,1,0))
    //                        * glm::vec4(camera_,1.0)) ;
    //    + centre_;
cout<<"azimuth "<<azimuthAngle_<<endl;
    azimuthAngle_ = angle_rads;

    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );

    //    // Our ModelViewProjection : multiplication of our 3 matrices
    //    MVP_ = mProjection_ * mView_ * mModel_; // Remember, matrix multiplication is the other way around
    setMVP();
    //    glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP_[0][0]);

    update();

}




void GLrenderer::resizeGL(int w, int h)
{
    // setup viewport, projection etc.:
    //    cout<<"wh "<<w<<" "<<h<<endl;
    width_=w;
    height_=h;
    mProjection_ = glm::perspective(glm::radians(45.0f), (float) width_ / (float)height_, 0.1f, 10000.0f);
    //    MVP_ = mProjection_ * mView_ * mModel_;
    setMVP();

    glBindRenderbuffer(GL_RENDERBUFFER,rbo_[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,w,h);

    glBindRenderbuffer(GL_RENDERBUFFER,rbo_[Color]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,w,h);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
    glViewport(0, 0, (GLint)w, (GLint)h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboAxial_[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,w,h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboAxial_[Color]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,w,h);

    glBindFramebuffer(GL_FRAMEBUFFER, fboAxial_);

    glViewport(0, 0, (GLint)w, (GLint)h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboSag_[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,w,h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboSag_[Color]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,w,h);

    glBindFramebuffer(GL_FRAMEBUFFER, fboSag_);

    glViewport(0, 0, (GLint)w, (GLint)h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboCor_[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,w,h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboCor_[Color]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,w,h);

    glBindFramebuffer(GL_FRAMEBUFFER, fboCor_);

    glViewport(0, 0, (GLint)w, (GLint)h);


    glBindRenderbuffer(GL_RENDERBUFFER,rboOblique_[Depth]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT,w,h);

    glBindRenderbuffer(GL_RENDERBUFFER,rboOblique_[Color]);
    glRenderbufferStorage(GL_RENDERBUFFER,GL_RGBA,w,h);

    glBindFramebuffer(GL_FRAMEBUFFER, fboOblique_);

    glViewport(0, 0, (GLint)w, (GLint)h);
}


void GLrenderer::renderAxial(){

    glBindFramebuffer(GL_FRAMEBUFFER, fboAxial_);
    glBindRenderbuffer(GL_RENDERBUFFER,rboAxial_[Color]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );

    if (imLoaded)
    {
        float x = image_.xsize()*0.5 * image_.xdim();
        float y = image_.ysize()*0.5 * image_.ydim();

        glm::mat4 MVP,mView;
        glm::vec3 camera{x,y,image_.zsize() * image_.zdim()+300}, centre{x,y,image_.zsize() * image_.zdim()},vup{0.0,1.0,0.0};

        mView = glm::lookAt(
                    camera, // Camera is at (4,3,3), in World Space
                    centre, // and looks at the origin
                    vup  //
                    );

        MVP = mProjection_ * mView * mModel_;


        glUseProgram(p_im_texture_);
        glUniformMatrix4fv(MVPid_image_, 1, GL_FALSE, &MVP[0][0]);



        cout<<"render axial plane"<<endl;

        glDepthMask(GL_TRUE);
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glEnable ( GL_TEXTURE_3D ) ;
        glActiveTexture(GL_TEXTURE0);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBindTexture(GL_TEXTURE_3D, texName_);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);


        glEnableVertexAttribArray(imageInVertexLoc_);
        glEnableVertexAttribArray(imageTexCoordLoc_);


        glVertexAttribPointer(
                    imageInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(8*sizeof(vertex<float>))            // array buffer offset
                    );
        glVertexAttribPointer(
                    imageTexCoordLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(8*sizeof(vertex<float>) + 3*sizeof(float))          // array buffer offset
                    );



        // Draw the triangles !
        //will always be 18, //3 planes * 2 triangles * 3 vertices/triangle
        //        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);
        glDrawElements(GL_TRIANGLES, 6,GL_UNSIGNED_INT,0);

        glDisableVertexAttribArray(imageInVertexLoc_);
        glDisableVertexAttribArray(imageTexCoordLoc_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, 0);

//    }
//    {
            //render surfaces
            glUseProgram(p_surface_);
            glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP[0][0]);

            glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
            //--------render surface

            AsrSurface<float> surfCam;
            surfCam.createSphere<float>(vec3<float>(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim()),5.0f,10,10);
            surfCam.bufferData(vertexBufferCamera_,vertexElementsBufferCamera_);


            // The following commands will talk about our 'vertexbuffer' buffer
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferCamera_);
            ////// Give our vertices to OpenGL.
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferCamera_);

            //// 1rst attribute buffer : vertices
            glEnableVertexAttribArray(surfaceInVertexLoc_);
            glEnableVertexAttribArray(surfaceInNormalLoc_);
            glEnableVertexAttribArray(surfaceInScalarLoc_);


            glVertexAttribPointer(
                        surfaceInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                        3,                  // size
                        GL_FLOAT,           // type
                        GL_FALSE,           // normalized?
                        sizeof(vertex<float>),                  // stride
                        (void*)0            // array buffer offset
                        );
            glVertexAttribPointer(surfaceInNormalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(3*sizeof(float)));
            glVertexAttribPointer(surfaceInScalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(6*sizeof(float)));


            glDrawElements(GL_TRIANGLES, surfCam.getNumberOfIndices(),GL_UNSIGNED_INT,0);

            glDisableVertexAttribArray(surfaceInVertexLoc_);
            glDisableVertexAttribArray(surfaceInNormalLoc_);
            glDisableVertexAttribArray(surfaceInScalarLoc_);

        }
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fboAxial_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width_, height_, width_/2, 0, width_*0.75, height_/2, GL_COLOR_BUFFER_BIT, GL_NEAREST);

}
void GLrenderer::renderSagittal(){

    glBindFramebuffer(GL_FRAMEBUFFER, fboSag_);
    glBindRenderbuffer(GL_RENDERBUFFER,rboSag_[Color]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );

    if (imLoaded)
    {
        cout<<"renderSag"<<endl;
        float z = image_.zsize()*0.5 * image_.zdim();
        float y = image_.ysize()*0.5 * image_.ydim();

        glm::mat4 MVP,mView;
        glm::vec3 camera{image_.xsize()*image_.xdim()-500,y,z}, centre{0,y,z},vup{0.0,0.0,1.0};

        mView = glm::lookAt(
                    camera, // Camera is at (4,3,3), in World Space
                    centre, // and looks at the origin
                    vup  //
                    );

        MVP = mProjection_ * mView * mModel_;


        glUseProgram(p_im_texture_);
        glUniformMatrix4fv(MVPid_image_, 1, GL_FALSE, &MVP[0][0]);



        cout<<"render sagittal plane"<<endl;

        glDepthMask(GL_TRUE);
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glEnable ( GL_TEXTURE_3D ) ;
        glActiveTexture(GL_TEXTURE0);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBindTexture(GL_TEXTURE_3D, texName_);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);


        glEnableVertexAttribArray(imageInVertexLoc_);
        glEnableVertexAttribArray(imageTexCoordLoc_);


        glVertexAttribPointer(
                    imageInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(4*sizeof(vertex<float>))            // array buffer offset
                    );
        glVertexAttribPointer(
                    imageTexCoordLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(4*sizeof(vertex<float>) + 3*sizeof(float))          // array buffer offset
                    );



        // Draw the triangles !
        //will always be 18, //3 planes * 2 triangles * 3 vertices/triangle
        //        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);
        glDrawElements(GL_TRIANGLES, 6,GL_UNSIGNED_INT,0);

        glDisableVertexAttribArray(imageInVertexLoc_);
        glDisableVertexAttribArray(imageTexCoordLoc_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, 0);

//    }
//    {
            //render surfaces
            glUseProgram(p_surface_);
            glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP[0][0]);

            glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
            //--------render surface

            AsrSurface<float> surfCam;
            surfCam.createSphere<float>(vec3<float>(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim()),5.0f,10,10);
            surfCam.bufferData(vertexBufferCamera_,vertexElementsBufferCamera_);


            // The following commands will talk about our 'vertexbuffer' buffer
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferCamera_);
            ////// Give our vertices to OpenGL.
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferCamera_);

            //// 1rst attribute buffer : vertices
            glEnableVertexAttribArray(surfaceInVertexLoc_);
            glEnableVertexAttribArray(surfaceInNormalLoc_);
            glEnableVertexAttribArray(surfaceInScalarLoc_);


            glVertexAttribPointer(
                        surfaceInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                        3,                  // size
                        GL_FLOAT,           // type
                        GL_FALSE,           // normalized?
                        sizeof(vertex<float>),                  // stride
                        (void*)0            // array buffer offset
                        );
            glVertexAttribPointer(surfaceInNormalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(3*sizeof(float)));
            glVertexAttribPointer(surfaceInScalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(6*sizeof(float)));


            glDrawElements(GL_TRIANGLES, surfCam.getNumberOfIndices(),GL_UNSIGNED_INT,0);

            glDisableVertexAttribArray(surfaceInVertexLoc_);
            glDisableVertexAttribArray(surfaceInNormalLoc_);
            glDisableVertexAttribArray(surfaceInScalarLoc_);

        }
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fboSag_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width_, height_, width_*0.5, height_*0.5, width_*0.75, height_, GL_COLOR_BUFFER_BIT, GL_NEAREST);
//    glBlitFramebuffer(0, 0, width_, height_, 0, 0, width_*0.75, height_, GL_COLOR_BUFFER_BIT, GL_NEAREST);

}
void GLrenderer::renderCoronal(){

    glBindFramebuffer(GL_FRAMEBUFFER, fboCor_);
    glBindRenderbuffer(GL_RENDERBUFFER,rboCor_[Color]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );

    if (imLoaded)
    {

        float x = image_.xsize()*0.5 * image_.xdim();
        float z = image_.zsize()*0.5 * image_.zdim();

        glm::mat4 MVP,mView;
        glm::vec3 camera{x,image_.ysize()*image_.ydim()-600,z}, centre{x,0,z},vup{0.0,0.0,1.0};

        mView = glm::lookAt(
                    camera, // Camera is at (4,3,3), in World Space
                    centre, // and looks at the origin
                    vup  //
                    );

        MVP = mProjection_ * mView * mModel_;


        glUseProgram(p_im_texture_);
        glUniformMatrix4fv(MVPid_image_, 1, GL_FALSE, &MVP[0][0]);

        cout<<"render axial plane"<<endl;

        glDepthMask(GL_TRUE);
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glEnable ( GL_TEXTURE_3D ) ;
        glActiveTexture(GL_TEXTURE0);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBindTexture(GL_TEXTURE_3D, texName_);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);


        glEnableVertexAttribArray(imageInVertexLoc_);
        glEnableVertexAttribArray(imageTexCoordLoc_);


        glVertexAttribPointer(
                    imageInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(0)            // array buffer offset
                    );
        glVertexAttribPointer(
                    imageTexCoordLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(3*sizeof(float))          // array buffer offset
                    );



        // Draw the triangles !
        //will always be 18, //3 planes * 2 triangles * 3 vertices/triangle
        //        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);
        glDrawElements(GL_TRIANGLES, 6,GL_UNSIGNED_INT,0);

        glDisableVertexAttribArray(imageInVertexLoc_);
        glDisableVertexAttribArray(imageTexCoordLoc_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, 0);

//    }
//    {
            //render surfaces
            glUseProgram(p_surface_);
            glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP[0][0]);

            glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
            //--------render surface

            AsrSurface<float> surfCam;
            surfCam.createSphere<float>(vec3<float>(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim()),5.0f,10,10);
            surfCam.bufferData(vertexBufferCamera_,vertexElementsBufferCamera_);


            // The following commands will talk about our 'vertexbuffer' buffer
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferCamera_);
            ////// Give our vertices to OpenGL.
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferCamera_);

            //// 1rst attribute buffer : vertices
            glEnableVertexAttribArray(surfaceInVertexLoc_);
            glEnableVertexAttribArray(surfaceInNormalLoc_);
            glEnableVertexAttribArray(surfaceInScalarLoc_);


            glVertexAttribPointer(
                        surfaceInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                        3,                  // size
                        GL_FLOAT,           // type
                        GL_FALSE,           // normalized?
                        sizeof(vertex<float>),                  // stride
                        (void*)0            // array buffer offset
                        );
            glVertexAttribPointer(surfaceInNormalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(3*sizeof(float)));
            glVertexAttribPointer(surfaceInScalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(6*sizeof(float)));


            glDrawElements(GL_TRIANGLES, surfCam.getNumberOfIndices(),GL_UNSIGNED_INT,0);

            glDisableVertexAttribArray(surfaceInVertexLoc_);
            glDisableVertexAttribArray(surfaceInNormalLoc_);
            glDisableVertexAttribArray(surfaceInScalarLoc_);

        }
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fboCor_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width_, height_, width_*0.75, height_*0.5, width_, height_, GL_COLOR_BUFFER_BIT, GL_NEAREST);

}

void GLrenderer::renderOblique(){

    glBindFramebuffer(GL_FRAMEBUFFER, fboCor_);
    glBindRenderbuffer(GL_RENDERBUFFER,rboCor_[Color]);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );

    if (imLoaded)
    {
        cout<<"Render Oblique image"<<endl;
        float x = image_.xsize()*0.5 * image_.xdim();
        float z = image_.zsize()*0.5 * image_.zdim();

        glm::mat4 MVP,mView;
        glm::vec3 centre{imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim()};
//        glm::vec3 centre{centre_.x]*image_.xdim(),centre_.y*image_.ydim(),imageSlices_[2]*image_.zdim()};

        glm::vec3 camera{centre.x + vup_.x *300 ,centre.y + vup_.y *300,centre.z + vup_.z *300};
//        glm::vec3 vup{0.0,0.0,1.0};


//        glm::vec3 vlook=   glm::normalize(centre - camera);

//        glm::vec3 vright = glm::cross(vlook, glm::vec3(0,-1,0));
        glm::vec3 vup(planeAxis_.x,planeAxis_.y,planeAxis_.z);
//        = glm::cross(vright, vlook);


        mView = glm::lookAt(
                    camera, // Camera is at (4,3,3), in World Space
                    centre, // and looks at the origin
                    vup  //
                    );

        MVP = mProjection_ * mView * mModel_;


        glUseProgram(p_im_texture_);
        glUniformMatrix4fv(MVPid_image_, 1, GL_FALSE, &MVP[0][0]);



        cout<<"render axial plane"<<endl;

        glDepthMask(GL_TRUE);
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glEnable ( GL_TEXTURE_3D ) ;
        glActiveTexture(GL_TEXTURE0);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBindTexture(GL_TEXTURE_3D, texName_);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);

        glEnableVertexAttribArray(imageInVertexLoc_);
        glEnableVertexAttribArray(imageTexCoordLoc_);


        glVertexAttribPointer(
                    imageInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(12*sizeof(vertex<float>))            // array buffer offset
                    );
        glVertexAttribPointer(
                    imageTexCoordLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(12*sizeof(vertex<float>) + 3*sizeof(float))          // array buffer offset
                    );

        // Draw the triangles !
        //will always be 18, //3 planes * 2 triangles * 3 vertices/triangle
        //        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);
        glDrawElements(GL_TRIANGLES, 6,GL_UNSIGNED_INT,BUFFER_OFFSET(0));

        glDisableVertexAttribArray(imageInVertexLoc_);
        glDisableVertexAttribArray(imageTexCoordLoc_);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, 0);

//    }

    //render camera
//    if (1==1){
        //render surfaces
        glUseProgram(p_surface_);
        glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP[0][0]);

        glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
        //--------render surface

        AsrSurface<float> surfCam;
        surfCam.createSphere<float>(vec3<float>(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim()),5.0f,10,10);
        surfCam.bufferData(vertexBufferCamera_,vertexElementsBufferCamera_);
        cout<<imageSlices_[0]*image_.xdim()<<" "<<imageSlices_[1]*image_.ydim()<<" "<<imageSlices_[2]*image_.zdim()<<endl;
//surfCam.write("/tmp/grot.vtk", AsrSurface<float>::VTK);

        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferCamera_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferCamera_);

        //// 1rst attribute buffer : vertices
        glEnableVertexAttribArray(surfaceInVertexLoc_);
        glEnableVertexAttribArray(surfaceInNormalLoc_);
        glEnableVertexAttribArray(surfaceInScalarLoc_);


        glVertexAttribPointer(
                    surfaceInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    (void*)0            // array buffer offset
                    );
        glVertexAttribPointer(surfaceInNormalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(3*sizeof(float)));
        glVertexAttribPointer(surfaceInScalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(6*sizeof(float)));


        glDrawElements(GL_TRIANGLES, surfCam.getNumberOfIndices(),GL_UNSIGNED_INT,0);

        glDisableVertexAttribArray(surfaceInVertexLoc_);
        glDisableVertexAttribArray(surfaceInNormalLoc_);
        glDisableVertexAttribArray(surfaceInScalarLoc_);

    }


    glBindFramebuffer(GL_READ_FRAMEBUFFER, fboCor_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width_, height_, width_*0.75, 0, width_, height_*0.5, GL_COLOR_BUFFER_BIT, GL_NEAREST);

}


void GLrenderer::paintGL()
{
    //    cout<<"paintGL"<<endl;
    setMVP();
    glUseProgram(p_surface_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_);
    glBindRenderbuffer(GL_DRAW_FRAMEBUFFER, rbo_[Color]);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT );

    if (imLoaded)
    {
        //----------render image plabes
        //        generateSlicePlanes();
        glUseProgram(p_im_texture_);
        glDepthMask(GL_TRUE);
        glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
        glEnable ( GL_TEXTURE_3D ) ;
        glActiveTexture(GL_TEXTURE0);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glBindTexture(GL_TEXTURE_3D, texName_);

        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);
        //// 1rst attribute buffer : vertices
        ///
        ///

        glEnableVertexAttribArray(imageInVertexLoc_);
        glEnableVertexAttribArray(imageTexCoordLoc_);


        glVertexAttribPointer(
                    imageInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    (void*)0            // array buffer offset
                    );
        glVertexAttribPointer(
                    imageTexCoordLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    BUFFER_OFFSET(3*sizeof(float))          // array buffer offset
                    );



        // Draw the triangles !
        //will always be 18, //3 planes * 2 triangles * 3 vertices/triangle
        glDrawElements(GL_TRIANGLES, 24,GL_UNSIGNED_INT,0);
//        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);

        glDisableVertexAttribArray(imageInVertexLoc_);
        glDisableVertexAttribArray(imageTexCoordLoc_);

        //        glTexCoord3f( (slice_position.x + half_vox_x)/v_im_dims[imageN].xsize,half_vox_y/v_im_dims[imageN].ysize,half_vox_z/v_im_dims[imageN].zsize);
        //        // glVertex3f( slice_position.x / xdim ,0 , 0); glNormal3f(0,1.0,0);
        //        glVertex3f( slice_position.x ,0 , 0); glNormal3f(0,1.0,0);

        //        glTexCoord3f( (slice_position.x + half_vox_x)/v_im_dims[imageN].xsize,half_vox_y/v_im_dims[imageN].ysize,(v_im_dims[imageN].zsize-half_vox_z) / v_im_dims[imageN].zsize);
        //        glVertex3f( slice_position.x , 0, (v_im_dims[imageN].zsize) - zdim );glNormal3f(0,1.0,0);

        //        glTexCoord3f( (slice_position.x + half_vox_x)/v_im_dims[imageN].xsize,(v_im_dims[imageN].ysize-half_vox_y) / v_im_dims[imageN].ysize,(v_im_dims[imageN].zsize-half_vox_z) / v_im_dims[imageN].zsize);
        //        glVertex3f( slice_position.x  , (v_im_dims[imageN].ysize) - ydim, (v_im_dims[imageN].zsize) - zdim );glNormal3f(0,1.0,0);

        //        glTexCoord3f( (slice_position.x + half_vox_x)/v_im_dims[imageN].xsize,(v_im_dims[imageN].ysize-half_vox_y) / v_im_dims[imageN].ysize,half_vox_z/v_im_dims[imageN].zsize);
        //        glVertex3f( slice_position.x  , (v_im_dims[imageN].ysize) - ydim  , 0);glNormal3f(0,1.0,0);


        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_3D, 0);
        //}





    }
    //    surfLoaded_=false;
    if (surfLoaded_)
    {
        //render surfaces
        glUseProgram(p_surface_);

        glPolygonMode(GL_FRONT_AND_BACK, glPolyMode_);
        //    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


        //--------render surface

        // The following commands will talk about our 'vertexbuffer' buffer
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer_);
        ////// Give our vertices to OpenGL.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBuffer_);
        //// 1rst attribute buffer : vertices
        glEnableVertexAttribArray(surfaceInVertexLoc_);
        glEnableVertexAttribArray(surfaceInNormalLoc_);
        glEnableVertexAttribArray(surfaceInScalarLoc_);

        glVertexAttribPointer(
                    surfaceInVertexLoc_,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                    3,                  // size
                    GL_FLOAT,           // type
                    GL_FALSE,           // normalized?
                    sizeof(vertex<float>),                  // stride
                    (void*)0            // array buffer offset
                    );
        glVertexAttribPointer(surfaceInNormalLoc_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(3*sizeof(float)));
        glVertexAttribPointer(surfaceInScalarLoc_, 1, GL_FLOAT, GL_FALSE, sizeof(vertex<float>), BUFFER_OFFSET(6*sizeof(float)));


        // Draw the triangles !
        //    cout<<"number of tris "<<surface_.numberOfVertices()<<" "<< surface_.numberOfIndices()<<endl;
        //    if (0==1)    glDrawElements(GL_TRIANGLES, 3,GL_UNSIGNED_INT,0);
//        glDrawElements(GL_TRIANGLES, 18,GL_UNSIGNED_INT,0);

        //if (1==1)
        glDrawElements(GL_TRIANGLES, surface_.getNumberOfIndices(),GL_UNSIGNED_INT,0);


        //    glDrawElements(GL_TRIANGLES, surface_.numberOfIndices(),GL_UNSIGNED_INT,0);
        glDisableVertexAttribArray(surfaceInVertexLoc_);
        glDisableVertexAttribArray(surfaceInNormalLoc_);
        glDisableVertexAttribArray(surfaceInScalarLoc_);


    }



    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glBlitFramebuffer(0, 0, width_, height_, 0, 0, width_/2, height_, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    //    glBlitFramebuffer(0, 0, width_, height_, width_/2, 0, width_*0.75, height_/2, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    //    glBlitFramebuffer(0, 0, width_, height_, width_/2, height_*0.5,width_*0.75, height_, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    //    glBlitFramebuffer(0, 0, width_, height_, width_*0.75, height_*0.5,width_,height_,  GL_COLOR_BUFFER_BIT, GL_NEAREST);
    //    glBlitFramebuffer(0, 0, width_, height_, width_*0.75, 0, width_, height_/2, GL_COLOR_BUFFER_BIT, GL_NEAREST);

    renderAxial();
    renderSagittal();
    renderCoronal();
    renderOblique();
    //    cout<<"paintGLend"<<endl;

//    QCoreApplication::processEvents();


}

void GLrenderer::setMVP()
{
//    ofstream fview("/tmp/view.mat",std::ofstream::out | std::ofstream::app);

        camera_ = cameraOrig_;
        centre_  = centreOrig_;
    glm::vec3 vlook=   glm::normalize(centreOrig_ - cameraOrig_);


glm::vec3 vright = glm::cross(vlook, glm::vec3(0,-1,0));
     vup_ = glm::cross(vright, vlook);


//azimuth
//    glm::vec3 vright =glm::normalize( glm::cross(vlook, vup_) );
//    vright glm::normalize(vright);
    cout<<"vup "<<vupOrig_.x<<" "<<vupOrig_.y<<" "<<vupOrig_.z<<endl;
    cout<<"vlook "<<vlook.x<<" "<<vlook.y<<" "<<vlook.z<<endl;
    cout<<"vright "<<vright.x<<" "<<vright.y<<" "<<vright.z<<endl;


    //these are not changing centre so don't have to sue centreOrig
    camera_ = glm::vec3( glm::rotate( azimuthAngle_, vup_)* glm::vec4(cameraOrig_ - centre_ ,1.0)) + centre_;



    vright = normalize(glm::cross(camera_ - centre_, vup_));
//    normalize(vright);
    camera_ =  glm::vec3(glm::rotate( elevationAngle_, vright)* glm::vec4(camera_ - centre_ ,1.0)) + centre_;
    vup_ = normalize(glm::cross(camera_ - centre_, vright));
//normalize(vup_);

//    fview<<"setMVP:vup "<<vup_.x<<" "<<vup_.y<<" "<<vup_.z<<endl;
//    fview<<"setMVP:camera "<<camera_.x<<" "<<camera_.y<<" "<<camera_.z<<endl;
//    fview<<"setMVP:centre "<<centre_.x<<" "<<centre_.y<<" "<<centre_.z<<endl;

    mView_ = glm::lookAt(
                camera_, // Camera is at (4,3,3), in World Space
                centre_, // and looks at the origin
                vup_  //
                );
//    for (int i = 0 ; i < 4 ; ++i){
//        for (int j = 0 ; j < 4; ++j ){
//            fview<<mView_[i][j]<<" ";
//        }
//        fview<<endl;
//    }
//   vupOrig_= vup_;


    //glm::mat4 test= glm::lookAt(glm::vec3(3,0,0),glm::vec3(3,0,1),glm::vec3(0,1,0));
    //std::cout<<glm::to_string(test)<<std::endl;
    cout<<"actualk mview "<<endl;

    MVP_ = mProjection_ * mView_ * mModel_;

    //The difference between the two is a permanent light vs one that follows the camera
    //        glm::vec4 ldir = glm::normalize(mView_ * glm::vec4(1.0f,1.0f,1.0f,0.0f));
    glm::vec3 ldir = glm::normalize( glm::vec3(0.0f,0.0f,1.0f));
    for (int i = 0 ; i < 4 ; ++i){
        for (int j = 0 ; j < 4; ++j ){
            cout<<" "<<mView_[i][j];
        }
        cout<<endl;
    }
    glm::mat3 NormalMatrix = glm::inverseTranspose(glm::mat3(mView_ * mModel_));
    glm::mat4 viewModelMatrix = (mView_ * mModel_);
    glUseProgram(p_surface_ );
    //    cout<<"normal id "<<Normal_id_surface_<<" "<<l_dir_id_<<endl;
    glUniformMatrix4fv(viewModel_id_surface_,1,GL_FALSE, &viewModelMatrix[0][0]);
    glUniformMatrix4fv(MVPid_surface_, 1, GL_FALSE, &MVP_[0][0]);
    glUniformMatrix3fv(Normal_id_surface_, 1, GL_FALSE, &NormalMatrix[0][0]);
    glUniform3fv(l_dir_id_, 1, &ldir[0]);

    glUseProgram(p_im_texture_);
    glUniformMatrix4fv(MVPid_image_, 1, GL_FALSE, &MVP_[0][0]);





}
void GLrenderer::createSurfacePrimitiveAtPlaneIntersection() {

    createSurfacePrimitive(glm::vec3(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim() ));

}
void GLrenderer::createSurfacePrimitiveAtOrigin(){
    createSurfacePrimitive(glm::vec3(0.0,0.0,0.0));
}
void GLrenderer::readSurface( const std::string & filename)
{
    AsrSurface<float> surf_in;
    surf_in.readVTK(filename);
    surface_.append(surf_in);

    setMVP();
    surface_.computeNeighbours();

    surface_.computeTriangleNeighbours();
    surface_.computeVertexNormals();
    cout<<"computeTriangleNormals..."<<endl;
    surface_.computeTriangleNormals();
    
    //    prepare for deformabel model
    //surface_.InitDeform();
    //surface_.printVertices();
    surface_.bufferData(vertexBuffer_,vertexElementsBuffer_);

    cout<<"done init deform"<<endl;
    surfLoaded_=true;
    update();
}
void GLrenderer::appendSurface( const Asr::Seg::AsrSurface<float> & surface )
{
    cout<<"append surface"<<endl;
    surface_.append(surface);
    surface_.computeNeighbours();

    surface_.computeTriangleNeighbours();
    surface_.computeVertexNormals();
    cout<<"computeTriangleNormals..."<<endl;
    surface_.computeTriangleNormals();

    //    prepare for deformabel model
    //surface_.InitDeform();
    //surface_.printVertices();
    surface_.bufferData(vertexBuffer_,vertexElementsBuffer_);
    update();

}

void GLrenderer::createSurfacePrimitive(const glm::vec3 & centre)
{
    cout<<"vertexBuffer "<<vertexBuffer_<<" "<<centre.x<<" "<<centre.y<<" "<<centre.z<<endl;
    //    if (surface_ == nullptr)
    //        surface_ = new aursurface<float>();
    //    surface_.createDiamondPrism(vec3<float>(centre.x,centre.y,centre.z),1.0f);
    surface_.createSphere(vec3<float>(centre.x,centre.y,centre.z),1.0f,10,10);

    surface_.bufferData(vertexBuffer_,vertexElementsBuffer_);

    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );
    //    MVP_ = mProjection_ * mView_ * mModel_;

    setMVP();


    //    prepare for deformabel model
    surface_.InitDeform();
    cout<<"done init deform"<<endl;
    surfLoaded_=true;
    update();

}

//void GLrenderer::generateSlice(){
//    //cout<<"texCoordX "<<texCoordX<<endl;
//    vector< vertex<float> > planes{ vertex<float>(0.1,0.1,1,0.0,texCoordY,0.0,0.0), \
//                vertex<float>(0.1,0.1,1,0.0,texCoordY,0.0,0.0), \
//                vertex<float>(0.1,0.1,1,0.0,texCoordY,0.0,0.0), \
//                vertex<float>(0.1,0.1,1,0.0,texCoordY,0.0,0.0)}

//}
void GLrenderer::generateSlicePlanes()
{
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferImage_);
    // Give our vertices to OpenGL.
       cout<<"generate plabe "<<image_.xdim()<<" "<<image_.ydim()<<" "<<image_.zdim()<<endl;
    // cout<<vertices_.size()<<" vretN "<<vertexElementsBuffer<<endl;
    vector<float> bounds{0,0,0,image_.xsize()*image_.xdim(),image_.ysize()*image_.ydim(),image_.zsize()*image_.zdim() };


    //calculate oblique slices
    vertex<float> centre(imageSlices_[0]*image_.xdim(),imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim());
    vertex<float> v000(bounds[0],bounds[1],bounds[2]);
    vertex<float> v001(bounds[0],bounds[1],bounds[5]);

    vertex<float> v100(bounds[3],bounds[1],bounds[2]);
    vertex<float> v101(bounds[3],bounds[1],bounds[5]);

    vertex<float> v010(bounds[0],bounds[4],bounds[2]);
    vertex<float> v011(bounds[0],bounds[4],bounds[5]);


    vertex<float> v110(bounds[3],bounds[4],bounds[2]);
    vertex<float> v111(bounds[3],bounds[4],bounds[5]);

    vec3<float> vup(vup_.x,vup_.y, vup_.z);
    cout<<"vuip "<<vup.to_string()<<endl;
    cout<<"centre "<<centre.to_string()<<endl;
    //vup.x =0;
    //vup.y=0;
    //vup.z=1;
    vector< vertex<float> > intersectPoints{
        linePlaneIntersection(v000,v001,centre, vup),
                linePlaneIntersection(v100,v101,centre, vup),
                linePlaneIntersection(v110,v111,centre, vup),
                linePlaneIntersection(v010,v011,centre, vup),

                linePlaneIntersection(v000,v100,centre, vup),
                linePlaneIntersection(v010,v110,centre, vup),
                linePlaneIntersection(v001,v101,centre, vup),
                linePlaneIntersection(v011,v111,centre, vup),

                linePlaneIntersection(v000,v010,centre, vup),
                linePlaneIntersection(v100,v110,centre, vup),
                linePlaneIntersection(v001,v011,centre, vup),
                linePlaneIntersection(v101,v111,centre, vup)};


    vector< tuple<float, vertex<float> > > validPoints;
    validPoints.reserve(4);

     float xfac{image_.xdim() * image_.xsize()};
     float yfac{image_.ydim() * image_.ysize()};
     float zfac{image_.zdim() * image_.zsize()};




    for (auto& v : intersectPoints){
        cout<<"intersect "<<v.to_string()<<endl;
        if ((v.scalar >=0) && (v.scalar <1))
        {
            cout<<"push back "<<endl;
            v.nx = v.x / xfac;
            v.ny = v.y / yfac;
            v.nz = v.z / zfac;

            validPoints.push_back(make_tuple(0,v));
        }
    }
    vec3<float> mean;
    for (auto& v : validPoints){
        mean += vec3<float>(get<1>(v));
    }
    mean/=validPoints.size();
    //replace centre with mean
//    vec3<float>
            planeAxis_=  mean - centre ;
//        vec3<float>(get<1>(validPoints.back()).x - centre.x, get<1>(validPoints.back()).y - centre.y,get<1>(validPoints.back()).z - centre.z)};
//should be using a better triangulation
    for (auto& v : validPoints){
        cout<<"planeAxis "<<planeAxis_.to_string()<<endl;
        get<1>(v).scalar = dotProduct(vec3<float>(get<1>(v)),planeAxis_);
        cout<<"valid point "<<get<1>(v).to_string()<<endl;
    }

    std::sort(begin(validPoints),end(validPoints),[](tuple<float, vertex<float> > const &t1, tuple<float, vertex<float>> const &t2) {
        return get<0>(t1) < get<0>(t2); // or use a custom compare function
    });
    for (auto& v : validPoints){
        cout<<"valid point2 "<<get<0>(v)<<" "<<get<1>(v).to_string()<<endl;
    }

    // a hack to make iblique view work
    swap(validPoints[0],validPoints[1]);
//    vertex<float> obP0 = linePlaneIntersection(v000,v001,centre, vup);
//    obP0.nx =  obP0.x / image_.xdim() / image_.xsize();
//    obP0.ny =  obP0.y / image_.ydim() / image_.ysize();
//    obP0.nz =  obP0.z / image_.zdim() / image_.zsize();

//    vertex<float> obP1 = linePlaneIntersection(v100,v101,centre, vup);
//    obP1.nx =  obP1.x / image_.xdim() / image_.xsize();
//    obP1.ny =  obP1.y / image_.ydim() / image_.ysize();
//    obP1.nz =  obP1.z / image_.zdim() / image_.zsize();

//    vertex<float> obP2 = linePlaneIntersection(v110,v111,centre, vup);
//    obP2.nx =  obP2.x / image_.xdim() / image_.xsize();
//    obP2.ny =  obP2.y / image_.ydim() / image_.ysize();
//    obP2.nz =  obP2.z / image_.zdim() / image_.zsize();

//    vertex<float> obP3 = linePlaneIntersection(v010,v011,centre, vup);
//    obP3.nx =  obP3.x / image_.xdim() / image_.xsize();
//    obP3.ny =  obP3.y / image_.ydim() / image_.ysize();
//    obP3.nz =  obP3.z / image_.zdim() / image_.zsize();
//cout<<"oblique "<<endl;
//cout<<obP0.to_string()<<endl;
//cout<<obP1.to_string()<<endl;
//cout<<obP2.to_string()<<endl;
//cout<<obP3.to_string()<<endl;



//x-z plane, y-z plane, xy-plane
    float texCoordX = static_cast<float>(imageSlices_[0])/image_.xsize();
    float texCoordY = static_cast<float>(imageSlices_[1])/image_.ysize();
    float texCoordZ = static_cast<float>(imageSlices_[2])/image_.zsize();

    //cout<<"texCoordX "<<texCoordX<<endl;
    vector< vertex<float> > planes{ vertex<float>(bounds[0],imageSlices_[1]*image_.ydim(),bounds[2],0.0,texCoordY,0.0,0.0), \
                vertex<float>(bounds[0],imageSlices_[1]*image_.ydim(),bounds[5],0.0,texCoordY,1.0,0.0), \
                vertex<float>(bounds[3],imageSlices_[1]*image_.ydim(),bounds[5],1.0,texCoordY,1.0,0.0), \
                vertex<float>(bounds[3],imageSlices_[1]*image_.ydim(),bounds[2],1.0,texCoordY,0.0,0.0), \

                vertex<float>(imageSlices_[0]*image_.xdim(),bounds[1],bounds[2],texCoordX,0.0,0.0,0.0), \
                vertex<float>(imageSlices_[0]*image_.xdim(),bounds[1],bounds[5],texCoordX,0.0,1.0,0.0), \
                vertex<float>(imageSlices_[0]*image_.xdim(),bounds[4],bounds[5],texCoordX,1.0,1.0,0.0), \
                vertex<float>(imageSlices_[0]*image_.xdim(),bounds[4],bounds[2],texCoordX,1.0,0.0,0.0), \

                vertex<float>(bounds[0],bounds[1],imageSlices_[2]*image_.zdim(),0.0,0.0,texCoordZ,0.0), \
                vertex<float>(bounds[0],bounds[4],imageSlices_[2]*image_.zdim(),0.0,1.0,texCoordZ,0.0), \
                vertex<float>(bounds[3],bounds[4],imageSlices_[2]*image_.zdim(),1.0,1.0,texCoordZ,0.0), \
                vertex<float>(bounds[3],bounds[1],imageSlices_[2]*image_.zdim(),1.0,0.0,texCoordZ,0.0),\
get<1>(validPoints[0]),get<1>(validPoints[1]),get<1>(validPoints[2]),get<1>(validPoints[3]) };
////                obP0, obP1,obP2, obP3 \
//                                  };

    vector<unsigned int> planes_indices{ 0,1,2,0,2,3,\
                                         4,5,6,4,6,7, \
                                         8,9,10,8,10,11, \
                                         13,12,14,12,14,15 };
                                         //change because of 1/2 swap
//                                         12,13,14,13,14,15 \
                                       };//3 planes * 2 triangles * 3 vertices/triangle
    //TODO CHANGE STATIC DRAW
    glBufferData(GL_ARRAY_BUFFER,planes.size() *sizeof(vertex<float>), &(planes[0]), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexElementsBufferImage_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,planes_indices.size() *sizeof(unsigned int), &(planes_indices[0]), GL_STATIC_DRAW);

}


void GLrenderer::setCurrentX( const int & x )
{ imageSlices_[0] = x;
    generateSlicePlanes();
    update();
}
void GLrenderer::setCurrentY( const int & y  ) {
    imageSlices_[1] =y;
    generateSlicePlanes();
    update();
}
void GLrenderer::setCurrentZ( const int & z  ) {
    imageSlices_[2] =z;
    generateSlicePlanes();
    update();
}

void  GLrenderer::recentreView(const glm::vec3 & camera_new, const glm::vec3 & centre_new)
{
    camera_=camera_new;
    centre_=centre_new;
    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );
    //    MVP_ = mProjection_ * mView_ * mModel_;

    setMVP();

    update();
}
void GLrenderer::setCentreToSurfaceCentroid()
{
    vec3<float> centroid = surface_.centroid();
    centreOrig_ = cameraOrig_ = glm::vec3(centroid.x,centroid.y,centroid.z);
    cameraOrig_.x += 100;

    //    mView_ = glm::lookAt(
    //                camera_, // Camera is at (4,3,3), in World Space
    //                centre_, // and looks at the origin
    //                vup_  // Head is up (set to 0,-1,0 to look upside-down)
    //                );

    setMVP();
    //repaint();
    update();
}
void GLrenderer::setCentreToPlaneIntersection()
{
    centreOrig_ = glm::vec3(imageSlices_[0]*image_.xdim(), imageSlices_[1]*image_.ydim(),imageSlices_[2]*image_.zdim());

    cameraOrig_ = centreOrig_;
    cameraOrig_.x += 100;
    setMVP();
    //    repaint();

    update();

}
float GLrenderer::getImageQfac(){
    return image_.GetQfac();
}

Asr::Seg::vec3<float> GLrenderer::getImagePixDim(){
    return image_.GetPixDim();

}

std::array<float,9> GLrenderer::getImageR(){
    return image_.GetR();
}


Asr::Seg::vec3<float> GLrenderer::getImageOrigin()
{
    return image_.GetOrigin();
}

void GLrenderer::setCoordinateSystem( const COORDSYSTEM & coord )
{

    cout<<"set coord  "<<endl;
    csystem_ =coord;
    setCoordinateSystemTransform();
    update();
}

void GLrenderer::setCoordinateSystemTransform()
{
    glUseProgram(p_im_texture_);

    if (csystem_ == SCALEDVOXEL)
    {
        VOX2MM_ = glm::mat4(1.0);

    }else if (csystem_ == NIFTI){
        VOX2MM_[ 0 ][0] = R_[0];
        VOX2MM_[ 0 ][1] = R_[1];
        VOX2MM_[ 0 ][2] = R_[2]*qfac_;
        VOX2MM_[ 0 ][3] = qoffset_.x ;
        VOX2MM_[ 1 ][0] = R_[3];
        VOX2MM_[ 1 ][1] = R_[4];
        VOX2MM_[ 1 ][2] = R_[5]*qfac_;
        VOX2MM_[ 1 ][3] = qoffset_.y ;
        VOX2MM_[ 2 ][0] = R_[6];
        VOX2MM_[ 2 ][1] = R_[7];
        VOX2MM_[ 2 ][2] = R_[8]*qfac_;
        VOX2MM_[ 2 ][3] = qoffset_.z ;
        VOX2MM_[ 3 ][0] = 0;
        VOX2MM_[ 3 ][1] = 0;
        VOX2MM_[ 3 ][2] = 0;
        VOX2MM_[ 3 ][3] = 1;

    }else if (csystem_ == ITK ){
        VOX2MM_[ 0 ][0] = 1 ;
        VOX2MM_[ 0 ][1] = R_[1];
        VOX2MM_[ 0 ][2] = R_[2];
        VOX2MM_[ 0 ][3] = qoffset_.x *  (( R_[0] < 0 )? -1 : 1) ;
        VOX2MM_[ 1 ][0] = R_[3];
        VOX2MM_[ 1 ][1] = 1 ;// R_[4];
        VOX2MM_[ 1 ][2] = R_[5]*qfac_;
        VOX2MM_[ 1 ][3] = qoffset_.y * (( R_[4] < 0 )? -1 : 1) ;
        VOX2MM_[ 2 ][0] = R_[6];
        VOX2MM_[ 2 ][1] = R_[7];
        VOX2MM_[ 2 ][2] =1;//R_[8]*qfac_;
        VOX2MM_[ 2 ][3] = qoffset_.z * (( R_[8]*qfac_ < 0 )? -1 :1);
        VOX2MM_[ 3 ][0] = 0;
        VOX2MM_[ 3 ][1] = 0;
        VOX2MM_[ 3 ][2] = 0;
        VOX2MM_[ 3 ][3] = 1;



    }
    //    VOX2MM_[ 0 ][0] =-1;
    //    VOX2MM_[ 0 ][1] = 0;
    //    VOX2MM_[ 0 ][2] = 0;
    //    VOX2MM_[ 0 ][3] = 0 ;
    //    VOX2MM_[ 1 ][0] = 0 ;// R_[3];
    //    VOX2MM_[ 1 ][1] = R_[4];
    //    VOX2MM_[ 1 ][2] = R_[5]*qfac_;
    //    VOX2MM_[ 1 ][3] = qoffset_.y ;
    //    VOX2MM_[ 2 ][0] = ( R_[6] < 0 )? (-1 * R_[6]) : R_[6];//R_[6];
    //    VOX2MM_[ 2 ][1] = R_[7];
    //    VOX2MM_[ 2 ][2] = R_[8]*qfac_;
    //    VOX2MM_[ 2 ][3] = qoffset_.z ;
    //    VOX2MM_[ 3 ][0] = 0;
    //    VOX2MM_[ 3 ][1] = 0;
    //    VOX2MM_[ 3 ][2] = 0;
    //    VOX2MM_[ 3 ][3] = 1;


    for (int i =0 ; i < 4 ; ++i)
    {
        for (int j = 0 ; j<4;++j)
        {
            cout<<VOX2MM_[i][j]<<" ";
        }
        cout<<endl;
    }
    glUniformMatrix4fv(VOX2MMid_, 1, GL_TRUE, &VOX2MM_[0][0]);
}
void GLrenderer::setImageLevel( const float & level ){
    //assume 100 steps
    glUseProgram(p_im_texture_);
    glUniform1f(m_imageLevelID, level);
    //glUniform1f(m_imageWindowID,1.0);
    update();

}void GLrenderer::setImageWindow( const float & window ){
    //assume 100 steps
    glUseProgram(p_im_texture_);

    glUniform1f(m_imageWindowID, window);
    //glUniform1f(m_imageWindowID,1.0);
    update();

}
void GLrenderer::setImageOpacityThreshold( const float & opacity )
{
    glUseProgram(p_im_texture_);
    //cout<<"opacity "<<opacity<<endl;
    glUniform1f(m_imageOpacityThreshID, opacity);
    update();
}

void GLrenderer::readImage( const std::string & filename)
{
    cout<<"read "<<filename<<endl;
    image_.readImage(filename);
    image_.normalize();
    image_.info();
    imLoaded=true;
    R_= image_.GetR();
    qoffset_ = image_.GetOrigin();
    pixdims_ = image_.GetPixDim();
    qfac_ = image_.GetQfac();
    glUseProgram(p_im_texture_);
    glEnable ( GL_TEXTURE_3D ) ;
    glActiveTexture(GL_TEXTURE0);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_3D, texName_);
    cout<<"textureName "<<texName_<<endl;

        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    //    glTexImage3D(GL_TEXTURE_3D,0,GL_RGBA32F_ARB, image->xsize(),image->ysize(),image->zsize(),0,GL_RED,GL_FLOAT,intensities);
    //vector<float> ftest(8*8*8,0.5);
    //for (int i =0 ; i < 8*8*8; ++i) ftest[i]=static_cast<float>(i)/(8*8*8) ;
    // glTexImage3D(GL_TEXTURE_3D,0,GL_RGBA32F, 8,8,8,0,GL_RED,GL_FLOAT,&(ftest[0]));


    //let resample the image only for visualization
    //there are graphics cards limits on texture size
    //let's assume 256 x 256 x256 define in header
    float stepX = (image_.xsize()-1)*image_.xdim() / (glTexSizeX);
    float stepY = (image_.ysize()-1)*image_.ydim() / (glTexSizeY);
    float stepZ = (image_.zsize()-1)*image_.zdim() / (glTexSizeZ);

    cout<<"Resampling image for textue mapping"<<endl;
    vector<float> im_resampled(glTexSizeX*glTexSizeY*glTexSizeZ);
    auto i_im = im_resampled.begin();
    for ( int z =0 ; z < glTexSizeZ; ++z){
        for (int y =0 ; y < glTexSizeY; ++y){
            for (int x =0 ; x < glTexSizeX; ++x, ++i_im){
//                               cout<<"inter "<<x<<" "<<y<<" "<<z<<" "<<stepX<<endl;
//                               cout<<"interp "<<image_.interpolate(x*stepX,y*stepY, z*stepZ)<<endl;
                *i_im = image_.interpolate(x*stepX,y*stepY, z*stepZ);
            }

        }
    }
    cout<<"done storing image "<<endl;
    glTexImage3D(GL_TEXTURE_3D,0,GL_RGBA32F,glTexSizeX,glTexSizeY,glTexSizeZ,0,GL_LUMINANCE,GL_FLOAT,&(im_resampled[0]));

    glUniform1f(m_imageLevelID,0.5);
    glUniform1f(m_imageWindowID,1.0);

    //    glTexImage3D(GL_TEXTURE_3D,0,GL_RGBA32F, image_.xsize(),image_.ysize(),image_.zsize(),0,GL_LUMINANCE,GL_FLOAT,image_.data());
    setCoordinateSystemTransform();





    generateSlicePlanes();
    recentreView(glm::vec3(static_cast<float>(image_.xsize())/2+static_cast<float>(image_.ysize())/2,static_cast<float>(image_.ysize())/2,static_cast<float>(image_.zsize())/2),\
                 glm::vec3(static_cast<float>(image_.xsize())/2,static_cast<float>(image_.ysize())/2,static_cast<float>(image_.zsize())/2));


    update();
}
