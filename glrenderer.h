#ifndef GLRENDERER_H
#define GLRENDERER_H

#include <AsrSurface/AsrSurface.h>
#include <AsrImage3D/AsrImage3D.h>

#define GL_GLEXT_PROTOTYPES
#include <QtOpenGL>
#include <iostream>
#include <glm/glm.hpp>

#include <deformablesurface.h>
#include <segmentationexplorer.h>
class GLrenderer : public QGLWidget
        //class GLrenderer : public QOpenGLWidget
{
//    Q_OBJECT

public:
    GLrenderer(QWidget *parent);
    ~GLrenderer();

    enum COORDSYSTEM { SCALEDVOXEL, NIFTI, ITK };
    void setVertexBuffers(GLuint* vbos ) {std::cout<<"setvbos "<<vbos[0]<<" "<<vbos[1]<<std::endl; vertexBuffer_=vbos[0];vertexElementsBuffer_=vbos[1];}
//    void setCameraVertexBuffers(GLuint* vbos ) {std::cout<<"setvbos "<<vbos[0]<<" "<<vbos[1]<<std::endl; vertexBufferCamera_=vbos[0];vertexElementsBufferCamera_=vbos[1];}

    void Initialize();
    void zoomIn();
    void zoomOut();
    void moveDown();
    void moveUp();
    void moveLeft();
    void moveRight();
    void moveForward();
    void moveBackward();
    void setElevation( const float & angle);
    void setAzimuth( const float & angle);
    void setCameraCentre( const float & x, const float & y , const float & z );
    void setLookVector( const float & a, const float & b, const float & c , const float & d );
    int writeTIFF(const unsigned int &  width, const unsigned int & height, const std::string & filename  , const unsigned int & compression);
    void takeSnapShot( const std::string & filename );
    void recentreView(const glm::vec3 & camera_new, const glm::vec3 & centre_new);
    void setCentreToSurfaceCentroid();
    void setCentreToPlaneIntersection();
    void flyThroughTrachea();
    void setToFlyThrough();
    void setPolygonMode(const GLenum & mode) { glPolyMode_=mode; update(); }
    void setUseExplicitVup( const bool & val ){ m_useExplicitVup=val;  };
    //surface functions
    void setDeformableSurfaceWidget( DeformableSurface *deformableSurfaceWidget );
    void setSegmentationExplorerWidget( SegmentationExplorer* segmentationExplorerWidget );
    void createSurfacePrimitiveAtPlaneIntersection();
    void createSurfacePrimitiveAtOrigin();
    void createSurfacePrimitive(const glm::vec3 & centre);
    void updateCameraOrientaton();

    //underlying image
    void readSurface( const std::string & filename);
    void appendSurface( const Asr::Seg::AsrSurface<float> & surface );
    void setImageLevel( const float & level);
    void setImageWindow( const float & window );
    void setImageOpacityThreshold( const float & opacity );
    void readImage( const std::string & filename);
    int imSizeX() { return image_.xsize(); }
    int imSizeY() { return image_.ysize(); }
    int imSizeZ() { return image_.zsize(); }
    float  imDimX() { return image_.xdim(); }
    float imDimY() { return image_.ydim(); }
    float imDimZ() { return image_.zdim(); }
    Asr::Seg::vec3<float> getImageOrigin();
    Asr::Seg::vec3<float> getImagePixDim();
    std::array<float,9> getImageR();
    float getImageQfac();
    void setCoordinateSystemTransform();
    void setCoordinateSystem( const COORDSYSTEM & coord );

    int currentX() { return imageSlices_[0]; }
    int currentY() { return imageSlices_[1]; }
    int currentZ() { return imageSlices_[2]; }
    void setCurrentX( const int & x ) ;
    void setCurrentY( const int & y  );
    void setCurrentZ( const int & z  );
signals:
    void changeCamera();

private:

    //GL buffers
    enum { Color, Depth, NumRenderbuffers };
    GLuint fbo_,rbo_[NumRenderbuffers];
    GLuint fboAxial_,rboAxial_[NumRenderbuffers];
    GLuint fboSag_,rboSag_[NumRenderbuffers];
    GLuint fboCor_,rboCor_[NumRenderbuffers];
    GLuint fboOblique_,rboOblique_[NumRenderbuffers];

    //image textures
    GLuint texName_;
    GLuint texLoc_;

    //shaders for surfaces
    GLuint v_surface_, f_surface_;//shaders
    GLuint p_surface_ ; //shader program

    GLuint v_im_texture_, f_im_texture_;
    GLuint p_im_texture_;

    GLuint  surfaceInVertexLoc_,surfaceInNormalLoc_,surfaceInScalarLoc_,imageInVertexLoc_,imageTexCoordLoc_;


    //frame size
    int height_,width_;

    //camera,model and projection info
    glm::mat4 VOX2MM_,MVP_,mProjection_,mView_,mModel_;
    glm::vec3 camera_{10.0,0.0,0.0}, centre_{0.0,0.0,0.0},vup_{0.0,0.0,1.0};
    glm::vec3 cameraOrig_{10.0,0.0,0.0}, centreOrig_{0.0,0.0,0.0},vupOrig_{0.0,0.0,1.0};
    Asr::Seg::vec3<float> planeAxis_;
    float elevationAngle_{0},azimuthAngle_{0};

    GLuint MVPid_surface_, MVPid_image_,VOX2MMid_;
    GLuint m_imageLevelID, m_imageWindowID, m_imageOpacityThreshID;
    GLuint Normal_id_surface_,l_dir_id_;
    GLuint viewModel_id_surface_;
    float zoomSensitivity_{0.25};//frcation of length

    //data to render
    Asr::Seg::AsrSurface<float> surface_;
    Asr::Seg::AsrImage3D<float> image_;
    std::vector<int> imageSlices_{0,0,0}; //slices postion for orthogonal slices
    bool imLoaded=false; //flag to only render if image has been loaded

    //Widgets
    DeformableSurface *deformableSurfaceWidget_{nullptr};
    SegmentationExplorer *m_SegmentationExplorerWidget{nullptr};
    bool m_useExplicitVup{true};

    bool surfLoaded_{false};

    Asr::Seg::vec3<float> qoffset_, pixdims_;
    std::array<float,9> R_;
    Asr::Seg::quaternion<float> qOrientation_;
    float qfac_{1};

//protected:

    static char* textFileRead(const char *fileName);

    void generateSlicePlanes();

    //GL
    void checkCompileShader( const GLuint & shader);
    void setShaders();
    void setMVP();
    void initializeGL();
    void setUpFrameRenderBuffers(GLuint* fbo , GLuint * rbo );
    void resizeGL(int w, int h);
    void renderAxial();
    void renderCoronal();
    void renderSagittal();
    void renderOblique();

    void paintGL();

    GLuint vertexBuffer_{0}, vertexElementsBuffer_{0};
    GLuint vertexBufferCamera_{0}, vertexElementsBufferCamera_{0};

    GLuint vertexBufferImage_{0}, vertexElementsBufferImage_{0};

    constexpr static int glTexSizeX{256},glTexSizeY{256},glTexSizeZ{256};
    GLenum glPolyMode_{GL_LINE};
    COORDSYSTEM csystem_{ITK};

};

#endif // GLRENDERER_H
