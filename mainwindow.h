#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QActionGroup>
#include <QString>
#include "deformablesurface.h"
#include "segmentationexplorer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    //GUI
//    void Initialize();
    void setGUIparameters(); //configure gui elements such as sliders, etc...
    void setUpMenu();//sets up connections to menu bar
    void setToFlyThrough();
     void flyThroughTrachea();
     //image manipulation
     void loadImage( const QString & filename );
     void addImage();
     void moveYZplane( int x );
     void moveXZplane( int y );
     void moveXYplane( int z );


    //surface manipulation
    void addSurface();
    void saveSurface();
    void loadSurface( const QString & filename );
    void loadPositionsAndQuaternions();
    void createMovie();
    void writeMovieFromImages( const std::string & imagePattern,  const std::string & ouputname, const int & fps);

    void nextPosAndQuat();
    void prevPosAndQuat();
    void addSkeleton( const QString & skelName );
    std::string imageCountToString( const int & count );

    void setCameraCentre();
    void setCameraCentre( const float & x, const float & y , const float & z );
    void movieCapture();

    void setLookVector();
    void setLookVector( const float & a, const float & b, const float & c, const float & d );

    void createSurfacePrimitive();
    void createSurfacePrimitiveAtPlaneIntersection();

    void createPrimitive( const float & x, const float & y, const float & z);

void setPolygonModeToLINE();
void setPolygonModeToFILL();
    //scene navigation
    void zoomIn();
    void zoomOut();
    void resetView();
    void setElevation( int angle );
    void setAzimuth( int angle );
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void moveForward();
    void moveBackward();
    void screenCapture();
    void setCentreToSurfaceCentroid();
    void setCentreToPlaneIntersection();
    void setCoordinateSystem(int coord);
    void adjustImageLevel( int level );
    void adjustImageWindow( int window );
    void adjustImageOpacityThreshold( int opacity );
private:
    void setPosition( const std::vector< Asr::Seg::vec3<float> >::iterator &  i_pos);
    void setQuaternion( const std::vector< Asr::Seg::quaternion<float> >::iterator &  i_q);
     Asr::Seg::vec3<float>  getWorldCoord(const int & x, const int & y , const int & z );
    void setWorldCoord( const Asr::Seg::vec3<float> & v_coord );
    void setWorldCoord();
    static Asr::Seg::AsrSurface<float> generateLocalCoordinateFrames( const std::vector<Asr::Seg::vec3<float> >& positions, const std::vector<Asr::Seg::quaternion<float> >& quaternion);

    std::vector<Asr::Seg::vec3<float> > m_positions;
    std::vector<Asr::Seg::quaternion<float> > m_quaternions;
    std::vector<Asr::Seg::vec3<float> >::iterator m_i_pos;
    std::vector<Asr::Seg::quaternion<float> >::iterator m_i_q;


    Ui::MainWindow *ui;
    DeformableSurface *deformableSurfaceWidget_{nullptr};
    SegmentationExplorer *m_segmentationExplorerWidget{nullptr};
    QActionGroup* GrpPolygonFill;
    Asr::Seg::vec3<float> qoffset_, pixdims_;
    std::array<float,9> R_;
    int qfac_{1};

};

#endif // MAINWINDOW_H
