#-------------------------------------------------
#
# Project created by QtCreator 2016-05-12T00:57:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QT += opengl
CXXFLAGS +=  -DGL_GLEXT_PROTOTYPES -DASR_SEG_USE_OPENGL
TARGET = visual-seg-tool
TEMPLATE = app
DEFINES += "ASR_SEG_USE_OPENGL"

SOURCES += main.cpp\
        mainwindow.cpp \
    glrenderer.cpp \
    deformablesurface.cpp \
    segmentationexplorer.cpp

INCLUDEPATH = ${HOME}/dev/Segmentation/ \
${HOME}/dev/Segmentation/AsrSegStructs/ \
${HOME}/dev/Segmentation/AsrImage3D/ \
${HOME}/dev/Segmentation/AsrGeometry/ \
${HOME}/dev/Segmentation/AsrTreeModel/ \
${HOME}/dev/Segmentation/AsrSurface/ \
/usr/include/nifti \
/usr/local/include/eigen3 \
${ASRSRC}/Main/Lib/AsrBase/Inc
LIBS = -L${HOME}/BUILDS/AsrSegCLI/Application \
        -lAsrTreeModel \
        -lAsrSurface \
        -lAsrImage3D \
        -lAsrGeometry \
        -lAsrBase \
        -lniftiio \
        -ltiff \
        -lprotobuf
       # -lGLU
HEADERS  += mainwindow.h \
    glrenderer.h \
    deformablesurface.h \
    segmentationexplorer.h

FORMS    += mainwindow.ui \
    deformablesurface.ui \
    segmentationexplorer.ui

DISTFILES += \
    glsl_shaders/basic_shader.vert \
    glsl_shaders/basic_shader.frag \
    glsl_shaders/image_texture.vert \
    glsl_shaders/image_texture.frag \
    glsl_shaders/grot.vert \
    glsl_shaders/grot.frag
