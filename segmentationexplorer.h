#ifndef SEGMENTATIONEXPLORER_H
#define SEGMENTATIONEXPLORER_H

#include <AsrSurface/AsrSurface.h>
#include <AsrTreeModel/AsrTreeModel.h>
#include <QDockWidget>
#include <QTreeWidgetItem>


namespace Ui {
class SegmentationExplorer;
}

class SegmentationExplorer : public QDockWidget
{
    Q_OBJECT

public:
    explicit SegmentationExplorer(QWidget *parent = 0);
    ~SegmentationExplorer();

    int GetFlyThroughSpeed();
  public slots:
    void loadSkeleton();
    void loadSkeleton(const QString & filename);

    void setSkeleton( const QString & filename);
    void snapToTrachea();
    QTreeWidgetItem* CreateTreeItem( const unsigned int & branch );
    void AddChildren( QTreeWidgetItem* treeItemParent, const unsigned int & currentBranch  );
    void GoToCurrentCarina();
    void FlyToCurrentCarinaFromTrachea();
    Asr::Seg::AsrContour<float> GetFlyThroughPath() { return m_currentFlyThrough; }
signals:
    void flyThroughTrachea();
    void setToFlyThrough();

private:
    Asr::Seg::AsrTreeModel m_airwayTree;
    Asr::Seg::AsrSurface<float> m_skeleton;
    Ui::SegmentationExplorer *ui;
    Asr::Seg::AsrContour<float> m_currentFlyThrough;
};

#endif // SEGMENTATIONEXPLORER_H
