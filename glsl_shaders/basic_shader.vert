#version 130

//attribute float InScalar;
//attribute vec3 InNormal;

in vec4 InVertex;
in vec3 InNormal;
in float InScalar;

uniform mat4 MVP;
uniform mat3 NormalMatrix;
uniform mat4 viewModelMatrix;

out vec3 normal;
out vec4 eye;
out vec4 mat_color;
//out vec3 triangle;

vec2 scalarRange;

void main(void)
{

    mat_color = vec4(1.0f,1.0f,1.0f,1.0f);
    if (InScalar==1){
        mat_color = vec4(1.0f,0.0f,0.0f,1.0f);
    }else if (InScalar==2){
        mat_color = vec4(0.0f,1.0f,1.0f,1.0f);
//    }else if (InScalar > 2){
//    	  mat_color = vec4(0.0f,0.0f,1.0f,1.0f);
    }else if (InScalar >= 255){
        //decompose by color
        float remainder;
//        mat_color = vec4(0.0f,0.0f,InScalar/255.0,1.0f);
        float r = floor(InScalar/ 65536.0);
        remainder=InScalar - r * 65536.0;
//       remainder = InScalar;
        float g = floor(remainder/256.0);
//        remainder = remainder - g * 256.0;
        float b = remainder - g*256.0;
                //floor(remainder/256);
//        float a =255.0f;
//        float a = remainder - b*256;
        mat_color = vec4(r/255.0,g/255.0,b/255.0,1.0);
//     mat_color = vec4(0,0,b/255.0,1.0f);
    }
//triangle=mat_color.rgb;
      normal = normalize( NormalMatrix * InNormal);
    eye = -(viewModelMatrix * InVertex);
    gl_Position =  MVP*InVertex ;

}
