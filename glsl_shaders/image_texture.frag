#version 130
uniform sampler3D texture0;
uniform float level;
uniform float window;
uniform float opacityThresh;
in vec3 texCoord0;

void main(void)
{
//    float level = 0.1;
//    float window = 0.2;
  //  gl_FragColor = vec4(texCoord0.x,0.0f,1.0f,1.0f);
    //vec4 color=vec4(texture3D( texture0, texCoord0).r,0.0f,0.0f,1.0f);
    vec4 color=texture3D( texture0, texCoord0);
    if (color.r  < opacityThresh ){
        color.a=0.0f;
    }
    float low = (level - 0.5*window);
    float high =  (level + 0.5*window);
    if (color.r < low ){
        color.rgb = vec3(0.0,0.0,0.0);
    }else if (color.r > high){
        color.rgb = vec3(1.0,1.0,1.0);

    }else{
        float val = (color.r - low)/window;
                color.rgb = vec3(val,val,val);
    }

    //if (color.r < 0.1)
   //     color.a=0;
  //  color.rgb=texCoord0;
  //  color.g=texCoord0.x;
    gl_FragColor = color;

//    gl_FragColor =texture3D( texture0, texCoord0).rgba;
}
