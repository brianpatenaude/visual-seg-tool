#version 130

in vec4 InVertex;
in vec3 InTexCoord;
uniform mat4 MVP;
uniform mat4 VOX2WORLD;

out vec3 texCoord0;

void main(void)
{
    texCoord0 = InTexCoord;
    gl_Position =  MVP*VOX2WORLD*InVertex ; //ftransform();

}
