#version 130
//varying vec4 diffuse,ambientGlobal, ambient;
//varying vec3 normal,lightDir,halfVector;

uniform vec3 l_dir;
in vec3 normal;
in vec4 eye;
in vec4 mat_color;
//in vec3 triangle;


void main(void)
{
//   vec3 l_dir.z = 1.0f;
//    vec4 spec = vec4(0.0);
//   float shininess = 45.0f;
      // normalize both input vectors
      vec3 n = normalize(normal);
//      vec3 e = normalize(eye));
vec4 ambient = vec4(0.5f,0.5f,0.5f,1.0f);

if ( dot(n,l_dir) < 0 )
{
    n=-n;
}
// vec3 triangle = vec3(1.0,1.0,1.0);

// float wire_width=5;
// vec3 d = fwidth(triangle);
// vec3 tdist = smoothstep(vec3(0.0), d*wire_width, triangle);

      float intensity = max(dot(n,l_dir), 0.0);

      // if the vertex is lit compute the specular color
//      if (intensity > 0.0) {
          // compute the half vector
//          vec3 h = normalize(l_dir + e);
          // compute the specular term into spec
//          float intSpec = max(dot(h,n), 0.0);
//          spec = specular * pow(intSpec,shininess);
//      }
//      colorOut = max(intensity *  diffuse + spec, ambient);
//    vec4 color;
//    color.rgb = normal;
//    color.rgb = vec3(1.0f,0.0f,0.0f);
//    mat_color.r=normal.x;
//vec4 fill_color = vec4(1.0f,0.0f,0.0f,1.0f);
//vec4 wire_color = vec4(0.0f,1.0f,0.0f,1.0f);
//if(min(min(triangle.x, triangle.y), triangle.z) < wire_width/10.0) {
//        gl_FragColor = wire_color;
//    } else {
//        gl_FragColor = fill_color;
//    }
    gl_FragColor = (intensity *vec4(1.0,1.0,1.0,1.0)+ambient)*mat_color;
//    gl_FragColor = mix(wire_color, fill_color,min(min(tdist.x, tdist.y), tdist.z));
//    gl_FragColor = wire_color + m
    //min(min(tdist.x, tdist.y), tdist.z));

    //max(intensity *  diffuse + spec, ambient);
}

