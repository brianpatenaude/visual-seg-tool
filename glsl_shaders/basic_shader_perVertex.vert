#version 130

//attribute float InScalar;
//attribute vec3 InNormal;

in vec4 InVertex;
in vec3 normal;
in float scalar;

out vec4 color;
uniform mat4 MVP;
uniform mat3 NormalMatrix;
uniform vec3 l_dir;
void main(void)
{
//    color=vec4(0.1f,0.1f,0.1f,1.0f);
//   # vec3 normal,lightDir,halfVector;
//   # vec4 diffuse,ambientGlobal,ambient;
// vec4 mat_color;
    //vec3 l_dir = vec3(1.0f,0.0f,0.0f);
//    vec3 l_dir_man  = vec3(1.0f,1.0f,1.0f);
    vec4 ambient = vec4(0.2f,0.2f,0.2f,1.0f);

    vec4 diffuse = vec4(1.0f,1.0f,1.0f,1.0f);
    vec3 n = normalize( NormalMatrix * normal);
//    vec3 n = normalize(  normal);
//    vec3 n = vec3(0.0f,0.0f,1.0f);
//   float  NdotL = dot(n,normalize(l_dir));

//    //2-sided lighting
//    if (NdotL < 0.0)
//    {
//        n=-n;
//////        NdotL = dot(n,normalize(l_dir));
//    }
//    NdotL = max(NdotL,0.0);

//    if (NdotL > 0.0) {

//            color += diffuse * NdotL ;

//            halfV = normalize(halfVector);
//            NdotHV = max(dot(n,halfV),0.0);
//          color += mat_color * gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV,gl_FrontMaterial.shininess);

//    }




    float step=2.0f;
    float intensity = max(dot(n, l_dir), 0.0);
//   color.a=1.0f;
//   color.rgb = vec3(1.0f,0.0f,0.0f);
//   if (scalar>0)
//   {

//       //area = 5 will saturate red
//       if (scalar<0)
//       {
//           color.rgb=vec3(1.0f,0.0f,0.0f);
//       }else if (scalar < step)
//       {
//           //red->yellow
////           color.rgb = vec3(0.0f,0.0f,0.0f);
//           color.r = 1.0f;// - scalar/2.0f;
//           color.g = 0.0f + scalar/step;
//           color.b = 0.0f;

//       }else if (scalar < 2*step)
//       {
//           //yellow->green
////           color.rgb += vec3(0.0f,0.5f,0.0f)*(scalar-2);
//           color.r = 1.0f - (scalar-2.0f*step)/step;
//           color.g = 1.0f;
//           color.b = 0.0f + (scalar-2.0f*step)/step;

//       }else if (scalar < 3*step ){
//           color.r = 0.0f;
//           color.g = 1.0 - (scalar-3.0f*step)/step;
//           color.b = 1.0f;
////           color.rgb += vec3(0.0f,0.0f,1.0f)*(scalar-4);

//       }else{

//           color.rgb=vec3(0.0f,0.0f,1.0f);

//       }
//    }
  color = ambient + intensity * diffuse;
//  +ambient;
   //color = normalize(color);
//color = ambient;

//       color.rgb=normalize(color.rgb);
   //color.rgb=n.rgb;
//    vec3 n = normalize(m_normal * normal);
//      mat_color = vec4(0.333f,0.333f,0.333f,1.0f);
//     mat_color.rgb=normalize(mat_color.rgb);
   //  normal = normalize( InNormal);
//     normal = normalize(gl_NormalMatrix * InNormal);

//        lightDir = normalize(vec3(gl_LightSource[0].position));
//        halfVector = normalize(gl_LightSource[0].halfVector.xyz);

//        /* Compute the diffuse, ambient and globalAmbient terms */
////        diffuse =  mat_color * gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
//        //   diffuse+= gl_BackMaterial.diffuse * gl_LightSource[0].diffuse;
//        /* The ambient terms have been separated since one of them */
//        /* suffers attenuation */
////        ambient =  mat_color * gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
//        //    ambient =  gl_BackMaterial.ambient * gl_LightSource[0].ambient;
////        ambientGlobal =  mat_color *  gl_FrontMaterial.ambient * gl_LightModel.ambient;
//    ambient = vec4(0.333f,0.333f,0.333f,1.0f);

    gl_Position =  MVP*InVertex ; //ftransform();

}
