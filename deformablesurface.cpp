#include "deformablesurface.h"
#include "ui_deformablesurface.h"
#include <iostream>
using namespace std;
using namespace Asr::Seg;
DeformableSurface::DeformableSurface(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::DeformableSurface)
{
    ui->setupUi(this);
    connect(ui->bDeform,SIGNAL(pressed()),this,SLOT(deform()));
    connect(ui->bSplitTriangles,SIGNAL(pressed()),this,SLOT(splitTriangles()));
    connect(ui->bSplitTriangles,SIGNAL(pressed()),this,SLOT(splitTriangles()));

}

DeformableSurface::~DeformableSurface()
{
    delete ui;
}

void DeformableSurface::splitTriangles()
{
    surface_->splitTriangles(ui->spinAreaThreshold->value());
    surface_->bufferData(vertexBuffer_,vertexElementsBuffer_);
    emit     surfaceChanged();

}


int DeformableSurface::deform()
{
    if (surface_ == nullptr) return 1;
    cout<<"deform "<<endl;
    cout<<"image "<<image_->GetMinIntensity()<<" "<<image_->GetMaxIntensity()<<endl;
    cout<<"vbos "<<vertexBuffer_<<" "<<vertexElementsBuffer_<<endl;
    float range =image_->GetMaxIntensity() -image_->GetMinIntensity();
    surface_->setReferenceIntensity( (ui->l_refIntensity->text().toFloat() - image_->GetMinIntensity() ) / range  );
    surface_->setGradientThresholdNormalization( range );
    //include loop here with 1 defomration at each iteration to interactivekly visualize
    for (int i = 0 ; i < ui->spinNiterations->value(); ++i)
    {

//    surface_->deform(*image_,1,ui->spinAlpha_sn->value(),ui->spinAlpha_st->value(),ui->spinAlpha_area->value(),\
                     ui->spinAlpha_im->value(), ui->spinMaxStepSize->value(), ui->spinAreaThreshold->value(), AsrSurface<float>::NEG_GRAD);
   // surface_->translate(vec3<float>(0.1,0.1,0.1));
   // surface_->printVertices();
//    cout<<"done translate "<<endl;
    //surface_->copyRadiusToScalars();
    surface_->copyAnchoredVerticesToScalars();
//    surface_->copyAreaToScalars();
    cout<<"does surface self-intersect? : "<<surface_->selfIntersects()<<endl;
surface_->bufferData(vertexBuffer_,vertexElementsBuffer_);
//cout<<"done buffer data "<<endl;
//renderWidget->update();
//        update();
        emit     surfaceChanged();

    }

    emit     surfaceChanged();

return 0;
}
