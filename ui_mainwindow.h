/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "glrenderer.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen_Surface;
    QAction *actionSave_Surface;
    QAction *actionOpen_Image;
    QAction *actionCreate_Surface_Primitive;
    QAction *actionWire_Mesh;
    QAction *actionFilled_Polygons;
    QAction *actionReimport_GLSL_shader;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_10;
    GLrenderer *openGLWidget;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuSurfaces;
    QMenu *menuRendering;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *navigationWidget;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_9;
    QFrame *frameScreenCapture;
    QVBoxLayout *verticalLayout_3;
    QPushButton *bScreenShot;
    QHBoxLayout *horizontalLayout_21;
    QLabel *labelOutDir;
    QLineEdit *lOutDir;
    QHBoxLayout *horizontalLayout_20;
    QLabel *labelOutFile;
    QLineEdit *l_OutFile;
    QFrame *line;
    QLabel *label;
    QHBoxLayout *horizontalLayout_9;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *bZoomOut;
    QSlider *sElevation;
    QPushButton *bZoomIn;
    QSlider *sAzimuth;
    QPushButton *bResetView;
    QPushButton *bCaptureVideo;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_17;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *bUp;
    QPushButton *bForward;
    QHBoxLayout *horizontalLayout;
    QPushButton *bLeft;
    QSpacerItem *horizontalSpacer;
    QPushButton *bRight;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *bBackward;
    QPushButton *bDown;
    QSpacerItem *horizontalSpacer_3;
    QSlider *sliderSensitivity;
    QFrame *frame1;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *bSetCameraCentre;
    QLineEdit *l_cx;
    QLineEdit *l_cy;
    QLineEdit *l_cz;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *bSetLookQuaternion;
    QLineEdit *l_qa;
    QLineEdit *l_qb;
    QLineEdit *l_qc;
    QLineEdit *l_qd;
    QHBoxLayout *horizontalLayout_22;
    QPushButton *bPosAndQuat;
    QLineEdit *lPosAndQuaternions;
    QPushButton *bPrev;
    QPushButton *bNext;
    QPushButton *bCreateMovie;
    QHBoxLayout *horizontalLayout_23;
    QPushButton *bCentreToPlanes;
    QPushButton *bCentreToCentroid;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_8;
    QComboBox *cCoordSystem;
    QHBoxLayout *horizontalLayout_12;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_4;
    QDoubleSpinBox *origin_x;
    QDoubleSpinBox *origin_y;
    QDoubleSpinBox *origin_z;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_14;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_5;
    QComboBox *cOrientation;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_16;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QVBoxLayout *verticalLayout_6;
    QSlider *sliderImageLevel;
    QSlider *sliderImageWindow;
    QSlider *sliderImageOpacityThresh;
    QLabel *label_2;
    QFrame *frameImageSlicer;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lbImX;
    QSlider *sImX;
    QSpinBox *voxX;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lbImY;
    QSlider *sImY;
    QSpinBox *voxY;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lbImZ;
    QSlider *sImZ;
    QSpinBox *voxZ;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout_15;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QDoubleSpinBox *sbX;
    QDoubleSpinBox *sbY;
    QDoubleSpinBox *sbZ;
    QSpacerItem *horizontalSpacer_8;
    QFrame *line_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(767, 903);
        MainWindow->setAnimated(true);
        actionOpen_Surface = new QAction(MainWindow);
        actionOpen_Surface->setObjectName(QStringLiteral("actionOpen_Surface"));
        actionSave_Surface = new QAction(MainWindow);
        actionSave_Surface->setObjectName(QStringLiteral("actionSave_Surface"));
        actionOpen_Image = new QAction(MainWindow);
        actionOpen_Image->setObjectName(QStringLiteral("actionOpen_Image"));
        actionCreate_Surface_Primitive = new QAction(MainWindow);
        actionCreate_Surface_Primitive->setObjectName(QStringLiteral("actionCreate_Surface_Primitive"));
        actionWire_Mesh = new QAction(MainWindow);
        actionWire_Mesh->setObjectName(QStringLiteral("actionWire_Mesh"));
        actionWire_Mesh->setCheckable(true);
        actionWire_Mesh->setChecked(false);
        actionFilled_Polygons = new QAction(MainWindow);
        actionFilled_Polygons->setObjectName(QStringLiteral("actionFilled_Polygons"));
        actionFilled_Polygons->setCheckable(true);
        actionFilled_Polygons->setChecked(true);
        actionReimport_GLSL_shader = new QAction(MainWindow);
        actionReimport_GLSL_shader->setObjectName(QStringLiteral("actionReimport_GLSL_shader"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout_10 = new QHBoxLayout(centralWidget);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        openGLWidget = new GLrenderer(centralWidget);
        openGLWidget->setObjectName(QStringLiteral("openGLWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(openGLWidget->sizePolicy().hasHeightForWidth());
        openGLWidget->setSizePolicy(sizePolicy);

        horizontalLayout_10->addWidget(openGLWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 767, 19));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuSurfaces = new QMenu(menuBar);
        menuSurfaces->setObjectName(QStringLiteral("menuSurfaces"));
        menuRendering = new QMenu(menuBar);
        menuRendering->setObjectName(QStringLiteral("menuRendering"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        navigationWidget = new QDockWidget(MainWindow);
        navigationWidget->setObjectName(QStringLiteral("navigationWidget"));
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_9 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        frameScreenCapture = new QFrame(dockWidgetContents);
        frameScreenCapture->setObjectName(QStringLiteral("frameScreenCapture"));
        frameScreenCapture->setFrameShape(QFrame::StyledPanel);
        frameScreenCapture->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frameScreenCapture);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        bScreenShot = new QPushButton(frameScreenCapture);
        bScreenShot->setObjectName(QStringLiteral("bScreenShot"));

        verticalLayout_3->addWidget(bScreenShot);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        labelOutDir = new QLabel(frameScreenCapture);
        labelOutDir->setObjectName(QStringLiteral("labelOutDir"));

        horizontalLayout_21->addWidget(labelOutDir);

        lOutDir = new QLineEdit(frameScreenCapture);
        lOutDir->setObjectName(QStringLiteral("lOutDir"));

        horizontalLayout_21->addWidget(lOutDir);


        verticalLayout_3->addLayout(horizontalLayout_21);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        labelOutFile = new QLabel(frameScreenCapture);
        labelOutFile->setObjectName(QStringLiteral("labelOutFile"));

        horizontalLayout_20->addWidget(labelOutFile);

        l_OutFile = new QLineEdit(frameScreenCapture);
        l_OutFile->setObjectName(QStringLiteral("l_OutFile"));

        horizontalLayout_20->addWidget(l_OutFile);


        verticalLayout_3->addLayout(horizontalLayout_20);


        verticalLayout_9->addWidget(frameScreenCapture);

        line = new QFrame(dockWidgetContents);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_9->addWidget(line);

        label = new QLabel(dockWidgetContents);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_9->addWidget(label);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        frame_2 = new QFrame(dockWidgetContents);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        bZoomOut = new QPushButton(frame_2);
        bZoomOut->setObjectName(QStringLiteral("bZoomOut"));
        QIcon icon;
        icon.addFile(QStringLiteral("../../Downloads/1463397750_search.png"), QSize(), QIcon::Normal, QIcon::Off);
        bZoomOut->setIcon(icon);
        bZoomOut->setAutoRepeat(true);

        horizontalLayout_7->addWidget(bZoomOut);

        sElevation = new QSlider(frame_2);
        sElevation->setObjectName(QStringLiteral("sElevation"));
        sElevation->setMinimum(-899);
        sElevation->setMaximum(899);
        sElevation->setTracking(true);
        sElevation->setOrientation(Qt::Vertical);
        sElevation->setInvertedAppearance(false);
        sElevation->setInvertedControls(false);
        sElevation->setTickPosition(QSlider::TicksBothSides);
        sElevation->setTickInterval(450);

        horizontalLayout_7->addWidget(sElevation);

        bZoomIn = new QPushButton(frame_2);
        bZoomIn->setObjectName(QStringLiteral("bZoomIn"));
        bZoomIn->setIcon(icon);
        bZoomIn->setAutoRepeat(true);

        horizontalLayout_7->addWidget(bZoomIn);


        verticalLayout_2->addLayout(horizontalLayout_7);

        sAzimuth = new QSlider(frame_2);
        sAzimuth->setObjectName(QStringLiteral("sAzimuth"));
        sAzimuth->setMinimum(-1800);
        sAzimuth->setMaximum(1800);
        sAzimuth->setTracking(true);
        sAzimuth->setOrientation(Qt::Horizontal);
        sAzimuth->setTickPosition(QSlider::TicksBothSides);
        sAzimuth->setTickInterval(900);

        verticalLayout_2->addWidget(sAzimuth);

        bResetView = new QPushButton(frame_2);
        bResetView->setObjectName(QStringLiteral("bResetView"));

        verticalLayout_2->addWidget(bResetView);

        bCaptureVideo = new QPushButton(frame_2);
        bCaptureVideo->setObjectName(QStringLiteral("bCaptureVideo"));

        verticalLayout_2->addWidget(bCaptureVideo);


        horizontalLayout_9->addWidget(frame_2);

        frame = new QFrame(dockWidgetContents);
        frame->setObjectName(QStringLiteral("frame"));
        horizontalLayout_17 = new QHBoxLayout(frame);
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        bUp = new QPushButton(frame);
        bUp->setObjectName(QStringLiteral("bUp"));
        bUp->setAutoRepeat(true);

        horizontalLayout_6->addWidget(bUp);

        bForward = new QPushButton(frame);
        bForward->setObjectName(QStringLiteral("bForward"));

        horizontalLayout_6->addWidget(bForward);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        bLeft = new QPushButton(frame);
        bLeft->setObjectName(QStringLiteral("bLeft"));
        bLeft->setAutoRepeat(true);

        horizontalLayout->addWidget(bLeft);

        horizontalSpacer = new QSpacerItem(60, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        bRight = new QPushButton(frame);
        bRight->setObjectName(QStringLiteral("bRight"));
        bRight->setAutoRepeat(true);

        horizontalLayout->addWidget(bRight);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        bBackward = new QPushButton(frame);
        bBackward->setObjectName(QStringLiteral("bBackward"));

        horizontalLayout_8->addWidget(bBackward);

        bDown = new QPushButton(frame);
        bDown->setObjectName(QStringLiteral("bDown"));
        bDown->setAutoRepeat(true);

        horizontalLayout_8->addWidget(bDown);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_8);


        horizontalLayout_17->addLayout(verticalLayout);

        sliderSensitivity = new QSlider(frame);
        sliderSensitivity->setObjectName(QStringLiteral("sliderSensitivity"));
        sliderSensitivity->setMaximum(100);
        sliderSensitivity->setOrientation(Qt::Vertical);

        horizontalLayout_17->addWidget(sliderSensitivity);


        horizontalLayout_9->addWidget(frame);


        verticalLayout_9->addLayout(horizontalLayout_9);

        frame1 = new QFrame(dockWidgetContents);
        frame1->setObjectName(QStringLiteral("frame1"));
        frame1->setFrameShape(QFrame::StyledPanel);
        frame1->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame1);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        bSetCameraCentre = new QPushButton(frame1);
        bSetCameraCentre->setObjectName(QStringLiteral("bSetCameraCentre"));

        horizontalLayout_18->addWidget(bSetCameraCentre);

        l_cx = new QLineEdit(frame1);
        l_cx->setObjectName(QStringLiteral("l_cx"));
        l_cx->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(l_cx);

        l_cy = new QLineEdit(frame1);
        l_cy->setObjectName(QStringLiteral("l_cy"));
        l_cy->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(l_cy);

        l_cz = new QLineEdit(frame1);
        l_cz->setObjectName(QStringLiteral("l_cz"));
        l_cz->setAlignment(Qt::AlignCenter);

        horizontalLayout_18->addWidget(l_cz);


        verticalLayout_4->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        bSetLookQuaternion = new QPushButton(frame1);
        bSetLookQuaternion->setObjectName(QStringLiteral("bSetLookQuaternion"));

        horizontalLayout_19->addWidget(bSetLookQuaternion);

        l_qa = new QLineEdit(frame1);
        l_qa->setObjectName(QStringLiteral("l_qa"));
        l_qa->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(l_qa);

        l_qb = new QLineEdit(frame1);
        l_qb->setObjectName(QStringLiteral("l_qb"));
        l_qb->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(l_qb);

        l_qc = new QLineEdit(frame1);
        l_qc->setObjectName(QStringLiteral("l_qc"));
        l_qc->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(l_qc);

        l_qd = new QLineEdit(frame1);
        l_qd->setObjectName(QStringLiteral("l_qd"));
        l_qd->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(l_qd);


        verticalLayout_4->addLayout(horizontalLayout_19);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        bPosAndQuat = new QPushButton(frame1);
        bPosAndQuat->setObjectName(QStringLiteral("bPosAndQuat"));

        horizontalLayout_22->addWidget(bPosAndQuat);

        lPosAndQuaternions = new QLineEdit(frame1);
        lPosAndQuaternions->setObjectName(QStringLiteral("lPosAndQuaternions"));

        horizontalLayout_22->addWidget(lPosAndQuaternions);

        bPrev = new QPushButton(frame1);
        bPrev->setObjectName(QStringLiteral("bPrev"));
        bPrev->setAutoRepeat(true);

        horizontalLayout_22->addWidget(bPrev);

        bNext = new QPushButton(frame1);
        bNext->setObjectName(QStringLiteral("bNext"));
        bNext->setAutoRepeat(true);

        horizontalLayout_22->addWidget(bNext);

        bCreateMovie = new QPushButton(frame1);
        bCreateMovie->setObjectName(QStringLiteral("bCreateMovie"));

        horizontalLayout_22->addWidget(bCreateMovie);


        verticalLayout_4->addLayout(horizontalLayout_22);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        bCentreToPlanes = new QPushButton(frame1);
        bCentreToPlanes->setObjectName(QStringLiteral("bCentreToPlanes"));

        horizontalLayout_23->addWidget(bCentreToPlanes);

        bCentreToCentroid = new QPushButton(frame1);
        bCentreToCentroid->setObjectName(QStringLiteral("bCentreToCentroid"));

        horizontalLayout_23->addWidget(bCentreToCentroid);


        verticalLayout_4->addLayout(horizontalLayout_23);


        verticalLayout_9->addWidget(frame1);

        groupBox = new QGroupBox(dockWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_8 = new QVBoxLayout(groupBox);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        cCoordSystem = new QComboBox(groupBox);
        cCoordSystem->setObjectName(QStringLiteral("cCoordSystem"));

        verticalLayout_8->addWidget(cCoordSystem);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_11->addWidget(label_4);

        origin_x = new QDoubleSpinBox(groupBox);
        origin_x->setObjectName(QStringLiteral("origin_x"));
        origin_x->setMinimum(-10000);
        origin_x->setMaximum(10000);

        horizontalLayout_11->addWidget(origin_x);

        origin_y = new QDoubleSpinBox(groupBox);
        origin_y->setObjectName(QStringLiteral("origin_y"));
        origin_y->setMinimum(-10000);
        origin_y->setMaximum(10000);

        horizontalLayout_11->addWidget(origin_y);

        origin_z = new QDoubleSpinBox(groupBox);
        origin_z->setObjectName(QStringLiteral("origin_z"));
        origin_z->setEnabled(true);

        horizontalLayout_11->addWidget(origin_z);


        horizontalLayout_12->addLayout(horizontalLayout_11);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_6);


        verticalLayout_8->addLayout(horizontalLayout_12);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_13->addWidget(label_5);

        cOrientation = new QComboBox(groupBox);
        cOrientation->setObjectName(QStringLiteral("cOrientation"));

        horizontalLayout_13->addWidget(cOrientation);


        horizontalLayout_14->addLayout(horizontalLayout_13);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_7);


        verticalLayout_8->addLayout(horizontalLayout_14);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout_7->addWidget(label_7);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_7->addWidget(label_8);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_7->addWidget(label_9);


        horizontalLayout_16->addLayout(verticalLayout_7);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        sliderImageLevel = new QSlider(groupBox);
        sliderImageLevel->setObjectName(QStringLiteral("sliderImageLevel"));
        sliderImageLevel->setMaximum(1000);
        sliderImageLevel->setValue(500);
        sliderImageLevel->setOrientation(Qt::Horizontal);
        sliderImageLevel->setInvertedAppearance(false);
        sliderImageLevel->setInvertedControls(false);

        verticalLayout_6->addWidget(sliderImageLevel);

        sliderImageWindow = new QSlider(groupBox);
        sliderImageWindow->setObjectName(QStringLiteral("sliderImageWindow"));
        sliderImageWindow->setMaximum(1000);
        sliderImageWindow->setValue(500);
        sliderImageWindow->setOrientation(Qt::Horizontal);

        verticalLayout_6->addWidget(sliderImageWindow);

        sliderImageOpacityThresh = new QSlider(groupBox);
        sliderImageOpacityThresh->setObjectName(QStringLiteral("sliderImageOpacityThresh"));
        sliderImageOpacityThresh->setMaximum(1000);
        sliderImageOpacityThresh->setOrientation(Qt::Horizontal);

        verticalLayout_6->addWidget(sliderImageOpacityThresh);


        horizontalLayout_16->addLayout(verticalLayout_6);


        verticalLayout_8->addLayout(horizontalLayout_16);


        verticalLayout_9->addWidget(groupBox);

        label_2 = new QLabel(dockWidgetContents);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_9->addWidget(label_2);

        frameImageSlicer = new QFrame(dockWidgetContents);
        frameImageSlicer->setObjectName(QStringLiteral("frameImageSlicer"));
        frameImageSlicer->setFrameShape(QFrame::StyledPanel);
        frameImageSlicer->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frameImageSlicer);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lbImX = new QLabel(frameImageSlicer);
        lbImX->setObjectName(QStringLiteral("lbImX"));

        horizontalLayout_2->addWidget(lbImX);

        sImX = new QSlider(frameImageSlicer);
        sImX->setObjectName(QStringLiteral("sImX"));
        sImX->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(sImX);

        voxX = new QSpinBox(frameImageSlicer);
        voxX->setObjectName(QStringLiteral("voxX"));

        horizontalLayout_2->addWidget(voxX);


        verticalLayout_5->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        lbImY = new QLabel(frameImageSlicer);
        lbImY->setObjectName(QStringLiteral("lbImY"));

        horizontalLayout_3->addWidget(lbImY);

        sImY = new QSlider(frameImageSlicer);
        sImY->setObjectName(QStringLiteral("sImY"));
        sImY->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(sImY);

        voxY = new QSpinBox(frameImageSlicer);
        voxY->setObjectName(QStringLiteral("voxY"));

        horizontalLayout_3->addWidget(voxY);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        lbImZ = new QLabel(frameImageSlicer);
        lbImZ->setObjectName(QStringLiteral("lbImZ"));

        horizontalLayout_4->addWidget(lbImZ);

        sImZ = new QSlider(frameImageSlicer);
        sImZ->setObjectName(QStringLiteral("sImZ"));
        sImZ->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(sImZ);

        voxZ = new QSpinBox(frameImageSlicer);
        voxZ->setObjectName(QStringLiteral("voxZ"));

        horizontalLayout_4->addWidget(voxZ);


        verticalLayout_5->addLayout(horizontalLayout_4);

        label_3 = new QLabel(frameImageSlicer);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_5->addWidget(label_3);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_6 = new QLabel(frameImageSlicer);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_5->addWidget(label_6);

        sbX = new QDoubleSpinBox(frameImageSlicer);
        sbX->setObjectName(QStringLiteral("sbX"));
        sbX->setMinimum(-10000);
        sbX->setMaximum(10000);

        horizontalLayout_5->addWidget(sbX);

        sbY = new QDoubleSpinBox(frameImageSlicer);
        sbY->setObjectName(QStringLiteral("sbY"));
        sbY->setMinimum(-10000);
        sbY->setMaximum(10000);

        horizontalLayout_5->addWidget(sbY);

        sbZ = new QDoubleSpinBox(frameImageSlicer);
        sbZ->setObjectName(QStringLiteral("sbZ"));
        sbZ->setMinimum(-10000);
        sbZ->setValue(0);

        horizontalLayout_5->addWidget(sbZ);


        horizontalLayout_15->addLayout(horizontalLayout_5);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_8);


        verticalLayout_5->addLayout(horizontalLayout_15);


        verticalLayout_9->addWidget(frameImageSlicer);

        line_2 = new QFrame(dockWidgetContents);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_9->addWidget(line_2);

        navigationWidget->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(2), navigationWidget);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuSurfaces->menuAction());
        menuBar->addAction(menuRendering->menuAction());
        menuFile->addAction(actionOpen_Surface);
        menuFile->addAction(actionSave_Surface);
        menuFile->addSeparator();
        menuFile->addAction(actionOpen_Image);
        menuSurfaces->addAction(actionCreate_Surface_Primitive);
        menuSurfaces->addSeparator();
        menuSurfaces->addAction(actionWire_Mesh);
        menuSurfaces->addAction(actionFilled_Polygons);
        menuSurfaces->addSeparator();
        menuRendering->addAction(actionReimport_GLSL_shader);

        retranslateUi(MainWindow);

        cCoordSystem->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOpen_Surface->setText(QApplication::translate("MainWindow", "Open Surface", 0));
        actionSave_Surface->setText(QApplication::translate("MainWindow", "Save Surface", 0));
        actionOpen_Image->setText(QApplication::translate("MainWindow", "Open Image", 0));
        actionCreate_Surface_Primitive->setText(QApplication::translate("MainWindow", "Create Surface Primitive", 0));
        actionWire_Mesh->setText(QApplication::translate("MainWindow", "Wire Mesh", 0));
        actionFilled_Polygons->setText(QApplication::translate("MainWindow", "Filled Polygons", 0));
        actionReimport_GLSL_shader->setText(QApplication::translate("MainWindow", "Reimport GLSL shader ", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuSurfaces->setTitle(QApplication::translate("MainWindow", "Surfaces", 0));
        menuRendering->setTitle(QApplication::translate("MainWindow", "Rendering", 0));
        navigationWidget->setWindowTitle(QString());
        bScreenShot->setText(QApplication::translate("MainWindow", "Take Screen Shot", 0));
        labelOutDir->setText(QApplication::translate("MainWindow", "Output Directory", 0));
        labelOutFile->setText(QApplication::translate("MainWindow", "Filename", 0));
        label->setText(QApplication::translate("MainWindow", "Camera Navigation", 0));
        bZoomOut->setText(QApplication::translate("MainWindow", "Zoom Out", 0));
        bZoomIn->setText(QApplication::translate("MainWindow", "Zoom In", 0));
        bResetView->setText(QApplication::translate("MainWindow", "Reset View", 0));
        bCaptureVideo->setText(QApplication::translate("MainWindow", "Capture Video", 0));
        bUp->setText(QApplication::translate("MainWindow", "+z", 0));
        bForward->setText(QApplication::translate("MainWindow", "+y", 0));
        bLeft->setText(QApplication::translate("MainWindow", "-x", 0));
        bRight->setText(QApplication::translate("MainWindow", "+x", 0));
        bBackward->setText(QApplication::translate("MainWindow", "-y", 0));
        bDown->setText(QApplication::translate("MainWindow", "-z", 0));
        bSetCameraCentre->setText(QApplication::translate("MainWindow", "Set Camera Centre", 0));
        l_cx->setText(QApplication::translate("MainWindow", "0.0", 0));
        l_cy->setText(QApplication::translate("MainWindow", "0.0", 0));
        l_cz->setText(QApplication::translate("MainWindow", "0.0", 0));
        bSetLookQuaternion->setText(QApplication::translate("MainWindow", "Set Look Vector (Quaternion)", 0));
        l_qa->setText(QApplication::translate("MainWindow", "0", 0));
        l_qb->setText(QApplication::translate("MainWindow", "0", 0));
        l_qc->setText(QApplication::translate("MainWindow", "0", 0));
        l_qd->setText(QApplication::translate("MainWindow", "0", 0));
        bPosAndQuat->setText(QApplication::translate("MainWindow", "Load Postion and Quaternions", 0));
        bPrev->setText(QApplication::translate("MainWindow", "Previous", 0));
        bNext->setText(QApplication::translate("MainWindow", "Next", 0));
        bCreateMovie->setText(QApplication::translate("MainWindow", "Create Movie", 0));
        bCentreToPlanes->setText(QApplication::translate("MainWindow", "Set Centre To Plane Intersection", 0));
        bCentreToCentroid->setText(QApplication::translate("MainWindow", "Set Centre To Surface Centroid", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Image Information", 0));
        cCoordSystem->clear();
        cCoordSystem->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Scaled Voxel", 0)
         << QApplication::translate("MainWindow", "NIFTI", 0)
         << QApplication::translate("MainWindow", "ITK", 0)
        );
        label_4->setText(QApplication::translate("MainWindow", "Origin", 0));
        label_5->setText(QApplication::translate("MainWindow", "Orientation", 0));
        cOrientation->clear();
        cOrientation->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "RAI", 0)
         << QApplication::translate("MainWindow", "RPI", 0)
        );
        label_7->setText(QApplication::translate("MainWindow", "Level", 0));
        label_8->setText(QApplication::translate("MainWindow", "Window", 0));
        label_9->setText(QApplication::translate("MainWindow", "Opacity", 0));
        label_2->setText(QApplication::translate("MainWindow", "Image Navigation", 0));
        lbImX->setText(QApplication::translate("MainWindow", "x", 0));
        lbImY->setText(QApplication::translate("MainWindow", "y", 0));
        lbImZ->setText(QApplication::translate("MainWindow", "z", 0));
        label_3->setText(QApplication::translate("MainWindow", "Location", 0));
        label_6->setText(QApplication::translate("MainWindow", "mm", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
