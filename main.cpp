#include "mainwindow.h"
#include <QApplication>
#include <QString>
#include <iostream>

using namespace std;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

//    w.Initialize();
    int i_arg=1;
    cout<<"argc "<<argc<<endl;
    while (i_arg < argc)
    {
        cout<<argv[i_arg]<<endl;
        if (string(argv[i_arg]) == "-im")
        {
            cout<<"im "<<endl;
            w.loadImage(QString(argv[++i_arg]));
        }else if (string(argv[i_arg]) == "-pos"){

            float x=atof(argv[++i_arg]);
            float y=atof(argv[++i_arg]);
            float z=atof(argv[++i_arg]);
            w.setCameraCentre(x,y,z);
        }else if (string(argv[i_arg]) == "-q"){

            float a=atof(argv[++i_arg]);
            float b=atof(argv[++i_arg]);
            float c=atof(argv[++i_arg]);
            float d=atof(argv[++i_arg]);


            w.setLookVector(a,b,c,d);
        }else if (string(argv[i_arg]) == "-surf")
        {
            ++i_arg;
            while ((string(argv[i_arg]) != "-im") && (string(argv[i_arg]) != "-c")&& (string(argv[i_arg]) != "-q" )&& (string(argv[i_arg]) != "-pos" )  )
            {
                cout<<"load next surf"<<endl;
//                cout<<"laod surf "<<string(argv[i_arg+1])<<endl;
               w.loadSurface(QString(argv[i_arg++]));
//               ++i_arg;
//               cout<<"laod surf2 "<<string(argv[i_arg])<<endl;
               cout<<"iarg "<<i_arg<<" "<<argc<<endl;
                if (i_arg >= argc )
                    break;
            }
           cout<<argv[i_arg]<<endl;
               --i_arg;
           cout<<argv[i_arg]<<endl;

        }else if (string(argv[i_arg]) == "-c")
        {
            int x = atof(argv[++i_arg]);
            int y = atof(argv[++i_arg]);
            int z = atof(argv[++i_arg]);

            w.createPrimitive(x,y,z);
        }else if (string(argv[i_arg]) == "-skel")
        {
            w.addSkeleton(QString(argv[++i_arg]));
        }else{

            cerr<<"Invalid Argument "<<argv[i_arg]<<endl;
            exit (EXIT_FAILURE);
        }

        ++i_arg;
    }
w.setLookVector();
    return a.exec();
}
