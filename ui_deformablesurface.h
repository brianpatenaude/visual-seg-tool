/********************************************************************************
** Form generated from reading UI file 'deformablesurface.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEFORMABLESURFACE_H
#define UI_DEFORMABLESURFACE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DeformableSurface
{
public:
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_8;
    QLineEdit *l_refIntensity;
    QPushButton *bSetRefIntensity;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QDoubleSpinBox *spinAreaThreshold;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *bSplitTriangles;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *spinAlpha_sn;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QDoubleSpinBox *spinAlpha_st;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_5;
    QDoubleSpinBox *spinAlpha_area;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_6;
    QDoubleSpinBox *spinAlpha_im;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_7;
    QDoubleSpinBox *spinMaxStepSize;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_3;
    QSpinBox *spinNiterations;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *bDeform;

    void setupUi(QDockWidget *DeformableSurface)
    {
        if (DeformableSurface->objectName().isEmpty())
            DeformableSurface->setObjectName(QStringLiteral("DeformableSurface"));
        DeformableSurface->resize(435, 459);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout = new QVBoxLayout(dockWidgetContents);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox_3 = new QGroupBox(dockWidgetContents);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_8 = new QHBoxLayout(groupBox_3);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_8->addWidget(label_8);

        l_refIntensity = new QLineEdit(groupBox_3);
        l_refIntensity->setObjectName(QStringLiteral("l_refIntensity"));

        horizontalLayout_8->addWidget(l_refIntensity);

        bSetRefIntensity = new QPushButton(groupBox_3);
        bSetRefIntensity->setObjectName(QStringLiteral("bSetRefIntensity"));

        horizontalLayout_8->addWidget(bSetRefIntensity);


        verticalLayout->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(dockWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        spinAreaThreshold = new QDoubleSpinBox(groupBox_2);
        spinAreaThreshold->setObjectName(QStringLiteral("spinAreaThreshold"));
        spinAreaThreshold->setValue(1);

        horizontalLayout_4->addWidget(spinAreaThreshold);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        verticalLayout_3->addLayout(horizontalLayout_4);

        bSplitTriangles = new QPushButton(groupBox_2);
        bSplitTriangles->setObjectName(QStringLiteral("bSplitTriangles"));

        verticalLayout_3->addWidget(bSplitTriangles);


        verticalLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(dockWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        spinAlpha_sn = new QDoubleSpinBox(groupBox);
        spinAlpha_sn->setObjectName(QStringLiteral("spinAlpha_sn"));
        spinAlpha_sn->setMaximum(1);
        spinAlpha_sn->setSingleStep(0.01);
        spinAlpha_sn->setValue(0.01);

        horizontalLayout->addWidget(spinAlpha_sn);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        spinAlpha_st = new QDoubleSpinBox(groupBox);
        spinAlpha_st->setObjectName(QStringLiteral("spinAlpha_st"));
        spinAlpha_st->setMaximum(1);
        spinAlpha_st->setSingleStep(0.1);

        horizontalLayout_2->addWidget(spinAlpha_st);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        spinAlpha_area = new QDoubleSpinBox(groupBox);
        spinAlpha_area->setObjectName(QStringLiteral("spinAlpha_area"));
        spinAlpha_area->setMaximum(1);
        spinAlpha_area->setSingleStep(0.1);

        horizontalLayout_5->addWidget(spinAlpha_area);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_5->addWidget(label_5);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        spinAlpha_im = new QDoubleSpinBox(groupBox);
        spinAlpha_im->setObjectName(QStringLiteral("spinAlpha_im"));
        spinAlpha_im->setMinimum(-1);
        spinAlpha_im->setMaximum(1);
        spinAlpha_im->setSingleStep(0.1);

        horizontalLayout_6->addWidget(spinAlpha_im);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_6->addWidget(label_6);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        spinMaxStepSize = new QDoubleSpinBox(groupBox);
        spinMaxStepSize->setObjectName(QStringLiteral("spinMaxStepSize"));
        spinMaxStepSize->setValue(0.1);

        horizontalLayout_7->addWidget(spinMaxStepSize);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_7->addWidget(label_7);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        spinNiterations = new QSpinBox(groupBox);
        spinNiterations->setObjectName(QStringLiteral("spinNiterations"));
        spinNiterations->setMinimum(1);
        spinNiterations->setMaximum(1000);
        spinNiterations->setValue(1);

        horizontalLayout_3->addWidget(spinNiterations);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout_3);


        verticalLayout->addWidget(groupBox);

        bDeform = new QPushButton(dockWidgetContents);
        bDeform->setObjectName(QStringLiteral("bDeform"));
        bDeform->setAutoRepeat(true);

        verticalLayout->addWidget(bDeform);

        DeformableSurface->setWidget(dockWidgetContents);

        retranslateUi(DeformableSurface);

        QMetaObject::connectSlotsByName(DeformableSurface);
    } // setupUi

    void retranslateUi(QDockWidget *DeformableSurface)
    {
        DeformableSurface->setWindowTitle(QApplication::translate("DeformableSurface", "DeformableSurface", 0));
        groupBox_3->setTitle(QApplication::translate("DeformableSurface", "Speed Function", 0));
        label_8->setText(QApplication::translate("DeformableSurface", "Reference Intensity", 0));
        l_refIntensity->setText(QApplication::translate("DeformableSurface", "150.00", 0));
        bSetRefIntensity->setText(QApplication::translate("DeformableSurface", "set to COG", 0));
        groupBox_2->setTitle(QApplication::translate("DeformableSurface", "Trianglem Splitting ", 0));
        label_4->setText(QApplication::translate("DeformableSurface", "Area threshold", 0));
        bSplitTriangles->setText(QApplication::translate("DeformableSurface", "Split Triangles", 0));
        groupBox->setTitle(QApplication::translate("DeformableSurface", "Deformation Parameters ", 0));
        label->setText(QApplication::translate("DeformableSurface", "Smoothness Weighting", 0));
        label_2->setText(QApplication::translate("DeformableSurface", "Regularization Weighting", 0));
        label_5->setText(QApplication::translate("DeformableSurface", "Triangle Regularization Weighting", 0));
        label_6->setText(QApplication::translate("DeformableSurface", "Image Weighting", 0));
        label_7->setText(QApplication::translate("DeformableSurface", "Maximum Step Size", 0));
        label_3->setText(QApplication::translate("DeformableSurface", "Number of Iterations", 0));
        bDeform->setText(QApplication::translate("DeformableSurface", "Deform", 0));
    } // retranslateUi

};

namespace Ui {
    class DeformableSurface: public Ui_DeformableSurface {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEFORMABLESURFACE_H
