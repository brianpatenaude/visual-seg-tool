#include "segmentationexplorer.h"
#include "ui_segmentationexplorer.h"
#include <QFileDialog>
#include <QDir>
#include <iostream>
#include <vector>
#include <QTreeWidget>
#include <QMessageBox>
#include <iomanip>
#include <AsrSegStructs/AsrSegStructs.h>
#include <AsrSegStructs/AsrSegStructs_functions.hpp>
#include <AsrSurface/AsrContour.h>

using namespace std;
using namespace Asr::Seg;

//using branch = AsrTreeModel::skeletonSegment;
SegmentationExplorer::SegmentationExplorer(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::SegmentationExplorer)
{
    ui->setupUi(this);
    connect(ui->bLoadSkeleton,SIGNAL(pressed()),this, SLOT(loadSkeleton()));
    connect(ui->bSnapToTrachea,SIGNAL(pressed()),this, SLOT(snapToTrachea()));
    connect(ui->bCurrentCarina,SIGNAL(pressed()),this, SLOT(GoToCurrentCarina()));
    connect(ui->bFlyFromTrachea,SIGNAL(pressed()),this, SLOT(FlyToCurrentCarinaFromTrachea()));


}

SegmentationExplorer::~SegmentationExplorer()
{
    delete ui;
}

void SegmentationExplorer::setSkeleton( const QString & filename)
{
    ui->lSkeletonName->setText(filename);
    loadSkeleton(filename);
}



QTreeWidgetItem* SegmentationExplorer::CreateTreeItem( const unsigned int & branch  ){
    QString ID;
    ID.setNum( get<AsrTreeModel::SKEL_ELEMENT::BRANCH_ID>(m_airwayTree.GetBranch(branch)) );
    QString Gen{ QString(to_string( get<AsrTreeModel::SKEL_ELEMENT::GENERATION>(m_airwayTree.GetBranch(branch))).c_str()) };
    cout<<"ID "<<ID.toStdString()<<endl;
    QString EndPoint{ QString( get<AsrTreeModel::SKEL_ELEMENT::END_POINT>(m_airwayTree.GetBranch(branch)).to_string(1).c_str()) };
    QString BranchPoint{ QString( get<AsrTreeModel::SKEL_ELEMENT::BRANCH_POINT>(m_airwayTree.GetBranch(branch)).to_string(1).c_str()) };
    QString MajorAxis{ QString( to_string(get<AsrTreeModel::SKEL_ELEMENT::ENTRY_MAJOR_AXIS>(m_airwayTree.GetBranch(branch))).c_str()) };
    QString MinorAxis{ QString( to_string(get<AsrTreeModel::SKEL_ELEMENT::ENTRY_MINOR_AXIS>(m_airwayTree.GetBranch(branch))).c_str()) };

    QStringList airway { ID, Gen, EndPoint, BranchPoint, MajorAxis, MinorAxis };
    QTreeWidgetItem* treeItem = new QTreeWidgetItem(airway);
    return treeItem;
}
void SegmentationExplorer::AddChildren( QTreeWidgetItem* treeItemParent, const unsigned int & currentBranch  ){

    QList<QTreeWidgetItem *> treeChildren;
    for (auto& child : m_airwayTree.GetChildren(currentBranch)){
        cout<<"child "<<currentBranch<<" "<<child<<endl;
        treeChildren.push_back(CreateTreeItem(child));
        AddChildren(treeChildren.back(),child);
    }
    if (!treeChildren.empty()){
        treeItemParent->addChildren(treeChildren);
        //    ui->treeWidget->topLevelItem(0)->addChildren(treeChildren);
    }

}
int SegmentationExplorer::GetFlyThroughSpeed(){
    return ui->sFlyThroughSpeed->value();//speed is delay in microseconds
}

void SegmentationExplorer::loadSkeleton(const QString & filename ){
    cout<<"load skeleton "<<endl;


    QString pointsFile = filename;
    //covers old and new naming conventions
    pointsFile.replace("tree.csv","tree.points.csv");
    pointsFile.replace("segment.csv","segment_cp.csv");

    if ( ! QFile::exists(pointsFile)){
        QMessageBox::warning(this, "File does not exist", pointsFile + " does not exist", QMessageBox::Yes);
    }
    ui->treeWidget->clear();

    cout<<"filename "<<filename.toStdString()<<endl;
    //    m_skeleton.readVTK(filename.toStdString());
    m_airwayTree.readTreeStructure(filename.toStdString());
    m_airwayTree.readTreePoints(pointsFile.toStdString());
    vector< skeletonSegment >  all_branches = m_airwayTree.GetSkeletonSegments();

    if ( m_airwayTree.GetNumberOfBranches() > 0){
        //start with trachea
        unsigned int currentBranch{1};

        ui->treeWidget->insertTopLevelItem(0,CreateTreeItem(currentBranch));
        //add children
        AddChildren(ui->treeWidget->topLevelItem(0),currentBranch);



    }

    cout<<"Number of branches "<<all_branches.size()<<endl;
    //    if (!all_branches.empty()){
    //        auto i_branch = all_branches.begin();
    //        cout<<"branch ID "<<get<AsrTreeModel::SKEL_ELEMENT::BRANCH_ID>(*i_branch);
    //    }
    std::unordered_map<int, int> m_id_to_parent;
    std::unordered_map<int, int> m_id_to_child;

}

void SegmentationExplorer::loadSkeleton(){
    cout<<"load skeleton "<<endl;

    QString filename = QFileDialog::getOpenFileName(this, tr("Open File"),QDir::current().absolutePath(), tr("skeleton (*.csv)"));
    ui->lSkeletonName->setText(filename);

    loadSkeleton(filename);
}
void SegmentationExplorer::GoToCurrentCarina(){
    cout<<"GoToCurrentCarina"<<endl;
    if (ui->treeWidget->selectedItems().size() > 0 ){
        cout<<"current branchg "<<ui->treeWidget->currentItem()->text(0).toInt()<<endl;
        AsrContour<float> fly_path;
        fly_path.push_back(to_vertex(get<AsrTreeModel::END_POINT>( m_airwayTree.GetBranch(ui->treeWidget->currentItem()->text(0).toInt()) )));
        fly_path.push_back(to_vertex(get<AsrTreeModel::BRANCH_POINT>( m_airwayTree.GetBranch(ui->treeWidget->currentItem()->text(0).toInt()) )));

        m_currentFlyThrough = fly_path;
        cout<<"got fflypath "<<fly_path.size()<<endl;
    }
    //          m_airwayTree.GetBranch(
    emit setToFlyThrough();
}
void SegmentationExplorer::FlyToCurrentCarinaFromTrachea(){
    cout<<"FlyToCurrentCarinaFromTrachea "<<endl;

    if (ui->treeWidget->selectedItems().size() > 0 ){
        cout<<"current branchg "<<ui->treeWidget->currentItem()->text(0).toInt()<<endl;
        AsrContour<float> fly_path;
        fly_path = m_airwayTree.GetPathToEndOfBranch(ui->treeWidget->currentItem()->text(0).toInt(),SmoothMethod::NONE);
        //        fly_path.push_back(to_vertex(get<AsrTreeModel::START_POINT>( m_airwayTree.GetBranch(1))));//start of trachea
        //        fly_path.push_back(to_vertex(get<AsrTreeModel::END_POINT>( m_airwayTree.GetBranch(ui->treeWidget->currentItem()->text(0).toInt()) )));
        //add final branch point
        fly_path.push_back(to_vertex(get<AsrTreeModel::BRANCH_POINT>( m_airwayTree.GetBranch(ui->treeWidget->currentItem()->text(0).toInt()) )));

        m_currentFlyThrough = fly_path;
    }
    //          m_airwayTree.GetBranch(
    emit setToFlyThrough();
}


void SegmentationExplorer::snapToTrachea(){
    cout<<"snap to trachea "<<endl;
    emit flyThroughTrachea();
}
