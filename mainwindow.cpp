#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <glrenderer.h>
#include <iostream>
#include <fstream>
//QT includes
#include <QFileDialog>
#include <QFile>
#include <QDir>
#include <Eigen/Core>

using namespace std;
using namespace Asr::Seg;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    GrpPolygonFill = new QActionGroup(this);
    GrpPolygonFill->addAction(ui->actionWire_Mesh);
    GrpPolygonFill->addAction(ui->actionFilled_Polygons);

    ui->actionWire_Mesh->setChecked(true);

    ui->bPrev->setEnabled(false);
    ui->bNext->setEnabled(false);

    setUpMenu();
    setGUIparameters();

    //this->addDockWidget(Qt::DockWidgetArea(0x2),imageContainer->getImageManipulatorWidget());

    connect(this->ui->bZoomIn,SIGNAL(pressed()),this,SLOT(zoomIn()));
    connect(this->ui->bZoomOut,SIGNAL(pressed()),this,SLOT(zoomOut()));
    connect(this->ui->bResetView,SIGNAL(pressed()),this,SLOT(resetView()));

    connect(this->ui->sElevation,SIGNAL(valueChanged(int)),this,SLOT(setElevation(int)));
    connect(this->ui->sAzimuth,SIGNAL(valueChanged(int)),this,SLOT(setAzimuth(int)));
    connect(this->ui->bDown,SIGNAL(pressed()),this,SLOT(moveDown()));
    connect(this->ui->bUp,SIGNAL(pressed()),this,SLOT(moveUp()));
    connect(this->ui->bLeft,SIGNAL(pressed()),this,SLOT(moveLeft()));
    connect(this->ui->bRight,SIGNAL(pressed()),this,SLOT(moveRight()));
    connect(this->ui->bForward,SIGNAL(pressed()),this,SLOT(moveForward()));
    connect(this->ui->bBackward,SIGNAL(pressed()),this,SLOT(moveBackward()));



    connect(this->ui->bScreenShot,SIGNAL(pressed()),this,SLOT(screenCapture()));
    connect(this->ui->bSetCameraCentre, SIGNAL(pressed()),this,SLOT(setCameraCentre()));
    connect(this->ui->bSetLookQuaternion, SIGNAL(pressed()),this,SLOT(setLookVector()));
    connect(this->ui->bPosAndQuat, SIGNAL(pressed()),this,SLOT(loadPositionsAndQuaternions()));
    connect(this->ui->bPrev, SIGNAL(pressed()),this,SLOT(prevPosAndQuat()));
    connect(this->ui->bNext, SIGNAL(pressed()),this,SLOT(nextPosAndQuat()));
    connect(this->ui->bCreateMovie, SIGNAL(pressed()),this,SLOT(createMovie()));
    connect(this->ui->bCaptureVideo,SIGNAL(pressed()), this, SLOT(movieCapture()));
    connect(this->ui->sImX,SIGNAL(valueChanged(int)),this,SLOT(moveYZplane(int)));
    connect(this->ui->sImY,SIGNAL(valueChanged(int)),this,SLOT(moveXZplane(int)));
    connect(this->ui->sImZ,SIGNAL(valueChanged(int)),this,SLOT(moveXYplane(int)));


    connect(ui->actionWire_Mesh,SIGNAL(triggered(bool)),this, SLOT(setPolygonModeToLINE()));
    connect(ui->actionFilled_Polygons,SIGNAL(triggered(bool)),this, SLOT(setPolygonModeToFILL()));
    connect(ui->cCoordSystem,SIGNAL(currentIndexChanged(int)),this, SLOT(setCoordinateSystem(int)));

    connect(ui->sliderImageLevel, SIGNAL(valueChanged(int)),this, SLOT(adjustImageLevel(int)));
    connect(ui->sliderImageWindow, SIGNAL(valueChanged(int)),this, SLOT(adjustImageWindow(int)));
    connect(ui->sliderImageOpacityThresh, SIGNAL(valueChanged(int)),this, SLOT(adjustImageOpacityThreshold(int)));

    deformableSurfaceWidget_ = new DeformableSurface();
    deformableSurfaceWidget_->setVisible(1);
    //    deformableSurfaceWidget_->setRenderWidget(ui->openGLWidget);
    m_segmentationExplorerWidget = new SegmentationExplorer();
    m_segmentationExplorerWidget->setVisible(1);

    connect(m_segmentationExplorerWidget,SIGNAL(setToFlyThrough()),this,SLOT(setToFlyThrough()));

    connect(m_segmentationExplorerWidget,SIGNAL(flyThroughTrachea()),this,SLOT(flyThroughTrachea()));
    tabifyDockWidget(this->ui->navigationWidget,deformableSurfaceWidget_);
    tabifyDockWidget(this->ui->navigationWidget,m_segmentationExplorerWidget);
    this->ui->navigationWidget->raise();
    ui->openGLWidget->setDeformableSurfaceWidget( deformableSurfaceWidget_ );

    ui->openGLWidget->setSegmentationExplorerWidget( m_segmentationExplorerWidget );

    ui->openGLWidget->Initialize();
    //ui->update();
    //update();
    //resize(100,600);
    //ui->openGLWidget->makeCurrent();
    //        resize(800,600);
    //ui->openGLWidget->updateGL();
    ui->lOutDir->setText(QDir::currentPath());
    ui->openGLWidget->update();
}
void MainWindow::setCoordinateSystem(int coord)
{
    cout<<"set coord main "<<coord<<endl;
    ui->openGLWidget->setCoordinateSystem(static_cast<GLrenderer::COORDSYSTEM>(coord));

}
void MainWindow::addSkeleton( const QString & skelName )
{
    m_segmentationExplorerWidget->setSkeleton(skelName);
}

void MainWindow::loadPositionsAndQuaternions()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QDir::currentPath(),
                                                    tr("Position and Quaternion Files (*.txt )"));
    if (filename.isEmpty())
    {
        return;
    }
    m_positions.clear();
    m_quaternions.clear();
    ui->lPosAndQuaternions->setText(filename);
    QFile file(filename);
    if (file.exists()){
        ifstream fin(filename.toStdString().c_str());
        if (fin.is_open()){
            string line;
            while (getline(fin,line)){
                cout<<"line "<<line<<endl;
                if (line.substr(0,1)=="["){
                    stringstream ss(line);
                    float a,b,c,d;
                    float x,y,z;
                    string val;
                    getline(ss, val, '[');
                    getline(ss, val, ',');
                    a=stof(val);
                    getline(ss, val, ',');
                    b=stof(val);
                    getline(ss, val, ',');
                    c=stof(val);
                    getline(ss, val, ',');
                    d=stof(val);
                    getline(ss, val, ',');
                    x=stof(val);
                    getline(ss, val, ',');
                    y=stof(val);
                    getline(ss, val, ']');
                    z=stof(val);
                    m_positions.push_back(Asr::Seg::vec3<float>(x,y,z));
                    m_quaternions.push_back(Asr::Seg::quaternion<float>(a,b,c,d));
                }
            }
            fin.close();
        }
    }

    m_i_pos = m_positions.begin();
    m_i_q = m_quaternions.begin();
    setPosition(m_i_pos);
    setQuaternion(m_i_q);
    ui->bPrev->setEnabled(false);

    if (m_positions.size()<=1){
        ui->bNext->setEnabled(false);

    }else{
        ui->bNext->setEnabled(true);
    }

    AsrSurface<float> tensors = generateLocalCoordinateFrames(m_positions,m_quaternions);
    QString sfilename = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    QDir::currentPath(),
                                                    tr("*.vtk"));
    tensors.write(sfilename.toStdString());


}

AsrSurface<float> MainWindow::generateLocalCoordinateFrames( const std::vector<Asr::Seg::vec3<float> >&  positions, const std::vector<Asr::Seg::quaternion<float> >& quaternion){
    AsrContour<float> pointsAndFrames;
    auto q =  quaternion.begin();
    for (auto& p : positions){

        pointsAndFrames.push_back(vertex<float>(p.x,p.y,p.z));
        float a{q->a};
        float b{q->b};
        float c{q->c};

        float d{q->d};
        array<float,9> R;
        R[0] = a * a + b * b - c * c - d * d;
        R[1] = 2 * b * c - 2 * a * d;
        R[2] = 2 * b * d + 2 * a * c;
        R[3] = 2 * b * c + 2 * a * d;
        R[4] = a * a + c * c - b * b - d * d;
        R[5] = 2 * c * d - 2 * a * b;
        R[6] = 2 * b * d - 2 * a * c;
        R[7] = 2 * c * d + 2 * a * b;
        R[8] = a * a + d * d - c * c - b * b;
        cout<<R[0]<<" "<<R[1]<<" "<<R[2]<<endl;
        cout<<R[3]<<" "<<R[4]<<" "<<R[5]<<endl;
        cout<<R[6]<<" "<<R[7]<<" "<<R[8]<<endl;
        //mView_[0][0] = a * a + b * b - c * c - d * d;
        //mView_[0][1] = 2 * b * c - 2 * a * d;
        //mView_[0][2] = 2 * b * d + 2 * a * c;
        //mView_[1][0] = 2 * b * c + 2 * a * d;
        //mView_[1][1] = a * a + c * c - b * b - d * d;
        //mView_[1][2] = 2 * c * d - 2 * a * b;
        //mView_[2][0] = 2 * b * d - 2 * a * c;
        //mView_[2][1] = 2 * c * d + 2 * a * b;
        //mView_[2][2] = a * a + d * d - c * c - b * b;*/
        //    float  dx=R[2];
        //    float  dy=R[5];
        //    float  dz=R[8];
        Eigen::Matrix<float, 3, 3> coordFrame;
        coordFrame(0,0)= R[2];
        coordFrame(1,0)= R[5];
        coordFrame(2,0)= R[8];

        //vup
        coordFrame(0,1)= R[1];
        coordFrame(1,1)= R[4];
        coordFrame(2,1)= R[7];

        //righ vector
        coordFrame(0,0)= R[0];
        coordFrame(1,0)= R[3];
        coordFrame(2,0)= R[6];


        //set to ortho
        coordFrame(0,0)= 0;
        coordFrame(1,0)= 0;
        coordFrame(2,0)= 1;

        //vup
        coordFrame(0,1)= 0;
        coordFrame(1,1)= 1;
        coordFrame(2,1)= 0;

        //righ vector
        coordFrame(0,2)= 1;
        coordFrame(1,2)= 0;
        coordFrame(2,2)= 0;
pointsAndFrames.push_back(coordFrame);

        ++q;
    }

    return pointsAndFrames.getTensorsAsArrows(1.0);
}


string MainWindow::imageCountToString( const int & count ){

    string frame;
    if (count < 10 ){
        frame="0000000" + to_string(count);
    }else if (count<100){
        frame="000000" + to_string(count);

    }else if (count<1000){
        frame="00000" + to_string(count);

    }else if (count<1000){
        frame="0000" + to_string(count);

    }else if (count<10000){
        frame="000" + to_string(count);

    }else{
        frame="00" + to_string(count);
    }
    return frame;
}
void MainWindow::createMovie(){

    QString movieName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                     QDir::currentPath(),
                                                     tr("*.mp4"));
    if (movieName.isEmpty()){
        return;
    }
    vector<string> images;
    int count=1;
    auto i_q = m_quaternions.begin();
    for (auto i_pos = m_positions.begin(); i_pos != m_positions.end(); ++i_pos, ++i_q,++count ){
        cout<<"frame "<<count<<endl;
        string frame = imageCountToString(count);
        //        if (count < 10 ){
        //            frame="0000000" + to_string(count);
        //        }else if (count<100){
        //            frame="000000" + to_string(count);

        //        }else if (count<1000){
        //            frame="00000" + to_string(count);

        //        }else if (count<1000){
        //            frame="0000" + to_string(count);

        //        }else if (count<10000){
        //            frame="000" + to_string(count);

        //        }else{
        //            frame="00" + to_string(count);
        //        }


        setPosition(i_pos);
        setQuaternion(i_q);
        ui->openGLWidget->repaint();
        string imname = "/tmp/movieFrame_"+frame+".tif";

        images.push_back(imname);
        ui->openGLWidget->takeSnapShot( imname );

    }

    writeMovieFromImages( "/tmp/movieFrame_%08d.tif", movieName.toStdString(),30);
    //    for (auto& f : images ){
    //        QDir::remove(QString(f.c_str()));

    //    }
}
void MainWindow::writeMovieFromImages( const string & imagePattern,  const string & outputname, const int & fps){
    QString program = "ffmpeg";
    QStringList arguments;
    QString stemp;
    //          int fps=30;
    stemp.setNum(fps);
    arguments<<"-y"<<"-i"<<QString(imagePattern.c_str())<<"-qmin"<<"1"<<"-qmax"<<"1"<<"-r"<<stemp<<QString(outputname.c_str());
    QProcess *myProcess = new QProcess();
    myProcess->setStandardOutputFile("/tmp/ffmpeg.txt");
    myProcess->setStandardErrorFile("/tmp/ffmpeg.log");
    myProcess->start(program, arguments);
    myProcess->waitForFinished(-1);
    cout<<"ffmpeg -y -i "<<imagePattern<<" -qmin 1 -qmax 1 -r "<<stemp.toStdString()<<" "<<outputname<<endl;

}

void MainWindow::nextPosAndQuat(){
    if ( (m_i_pos + 1) != m_positions.end() ){
        setPosition(++m_i_pos);
        setQuaternion(++m_i_q);
        ui->bPrev->setEnabled(true);
    }
}
void MainWindow::prevPosAndQuat(){
    if ( m_i_pos != m_positions.begin() ){
        setPosition(--m_i_pos);
        setQuaternion(--m_i_q);
        ui->bNext->setEnabled(true);
    }
}
void MainWindow::setPosition( const vector< vec3<float> >::iterator &  i_pos){
    QString snum;
    snum.setNum(i_pos->x);
    ui->l_cx->setText(snum);
    snum.setNum(i_pos->y);
    ui->l_cy->setText(snum);
    snum.setNum(i_pos->z);
    ui->l_cz->setText(snum);
    setCameraCentre();
}
void MainWindow::setQuaternion( const vector< quaternion<float> >::iterator &  i_q){
    QString snum;
    snum.setNum(i_q->a);
    ui->l_qa->setText(snum);
    snum.setNum(i_q->b);
    ui->l_qb->setText(snum);
    snum.setNum(i_q->c);
    ui->l_qc->setText(snum);
    snum.setNum(i_q->d);
    ui->l_qd->setText(snum);
    setLookVector();
}

void MainWindow::movieCapture(){
    cout<<"take snap shot"<<endl;

    QString movieName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                     QDir::currentPath(),
                                                     tr("*.mp4"));
    float aziOrig = ui->sAzimuth->value()/10.0;
    int count{1};
    for (int i = -90 ; i < 90; ++i, ++count )
    {
        ui->openGLWidget->setAzimuth(aziOrig + i);
        ui->openGLWidget->repaint();
        string filename = "/tmp/movieCapture_" + imageCountToString(count) + ".tif";

        ui->openGLWidget->takeSnapShot(filename);

    }

    if (!movieName.isEmpty()){
        cout<<"converting image to movie "<<movieName.toStdString()<<endl;
        writeMovieFromImages( "/tmp/movieCapture_%08d.tif", movieName.toStdString(),30);
    }
    //    if (ui->l_OutFile->text().isEmpty() || ui->lOutDir->text().isEmpty()){


    //        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
    //                                                    QDir::currentPath(),
    //                                                    tr("*.tif"));
    //        if (fileName.isEmpty()) return;
    //        QDir fileDir(fileName);
    ////        QFile qfilename (fileName);
    //        QString file(fileDir.dirName());
    //        fileName.remove(fileName.size() - file.size(), file.size());
    //        ui->l_OutFile->setText(file);
    //        ui->lOutDir->setText(fileName);

    //    }

}
void MainWindow::screenCapture(){
    cout<<"take snap shot"<<endl;

    if (ui->l_OutFile->text().isEmpty() || ui->lOutDir->text().isEmpty()){


        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                        QDir::currentPath(),
                                                        tr("*.tif"));
        if (fileName.isEmpty()) return;
        QDir fileDir(fileName);
        //        QFile qfilename (fileName);
        QString file(fileDir.dirName());
        fileName.remove(fileName.size() - file.size(), file.size());
        ui->l_OutFile->setText(file);
        ui->lOutDir->setText(fileName);

    }

    ui->openGLWidget->takeSnapShot( (ui->lOutDir->text() + "/" + ui->l_OutFile->text()).toStdString() );
}

void MainWindow::flyThroughTrachea()
{
    ui->openGLWidget->flyThroughTrachea();
}
void MainWindow::setToFlyThrough()
{
    ui->openGLWidget->setUseExplicitVup(false);
    ui->sAzimuth->setValue(0);
    ui->sElevation->setValue(0);
    ui->openGLWidget->setToFlyThrough();
    ui->openGLWidget->setUseExplicitVup(true);

}

void MainWindow::setGUIparameters()
{
    ui->sImX->setSingleStep(1);
    ui->sImY->setSingleStep(1);
    ui->sImZ->setSingleStep(1);

    ui->sbX->setMinimum(-10000);
    ui->sbX->setMaximum(10000);
    ui->sbY->setMinimum(-10000);
    ui->sbY->setMaximum(10000);
    ui->sbZ->setMinimum(-10000);
    ui->sbZ->setMaximum(10000);

    ui->origin_x->setMinimum(-10000);
    ui->origin_x->setMaximum(10000);
    ui->origin_y->setMinimum(-10000);
    ui->origin_y->setMaximum(10000);
    ui->origin_z->setMinimum(-10000);
    ui->origin_z->setMaximum(10000);

}


void  MainWindow::setPolygonModeToLINE()
{
    ui->openGLWidget->setPolygonMode(GL_LINE);
}

void  MainWindow::setPolygonModeToFILL()
{
    cout<<"set polygon mode"<<endl;
    ui->openGLWidget->setPolygonMode(GL_FILL);

}


void MainWindow::setUpMenu()
{


    //----------------------SETUP ACTION CONNECTIONS (Toolbar)------------------//
    connect(ui->actionOpen_Surface, SIGNAL(triggered()),this,SLOT(addSurface()));
    connect(ui->actionSave_Surface, SIGNAL(triggered()),this,SLOT(saveSurface()));
    connect(ui->actionOpen_Image, SIGNAL(triggered()),this,SLOT(addImage()));

    //-----surface actions--------------//
    //    connect(ui->actionCreate_Surface_Primitive,SIGNAL(triggered()),this,SLOT(createSurfacePrimitive()));
    connect(ui->actionCreate_Surface_Primitive,SIGNAL(triggered()),this,SLOT(createSurfacePrimitiveAtPlaneIntersection()));
    connect(ui->bCentreToCentroid,SIGNAL(pressed()),this,SLOT(setCentreToSurfaceCentroid()));
    connect(ui->bCentreToPlanes,SIGNAL(pressed()),this,SLOT(setCentreToPlaneIntersection()));



}



MainWindow::~MainWindow()
{
    if (deformableSurfaceWidget_!=nullptr)  delete deformableSurfaceWidget_;

    delete GrpPolygonFill;
    delete ui;
}

void MainWindow::zoomIn() { ui->openGLWidget->zoomIn(); }
void MainWindow::zoomOut() { ui->openGLWidget->zoomOut(); }

void MainWindow::resetView(){
    ui->sAzimuth->setValue(0);
    ui->sElevation->setValue(0);
    ui->openGLWidget->setElevation(0);
    ui->openGLWidget->setAzimuth(0);
}


void MainWindow::setElevation( int angle ) {  ui->openGLWidget->setElevation(static_cast<float>(angle)/10.0);}
void MainWindow::setAzimuth( int angle ) {  ui->openGLWidget->setAzimuth(static_cast<float>(angle)/10.0);}
void MainWindow::moveUp( ){ ui->openGLWidget->moveUp(); }
void MainWindow::moveDown( ){ ui->openGLWidget->moveDown(); }
void MainWindow::moveLeft( ){ ui->openGLWidget->moveLeft(); }
void MainWindow::moveRight( ){ ui->openGLWidget->moveRight(); }
void MainWindow::moveForward(){ ui->openGLWidget->moveForward(); }
void MainWindow::moveBackward(){ ui->openGLWidget->moveBackward(); }
void MainWindow::createSurfacePrimitive()
{
    cout<<"createSurfacePrimitive "<<endl;
    ui->openGLWidget->createSurfacePrimitiveAtOrigin();

}
void MainWindow::createSurfacePrimitiveAtPlaneIntersection()
{
    ui->openGLWidget->createSurfacePrimitiveAtPlaneIntersection();
}

void MainWindow::saveSurface()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Surface File"),
                                                    "/home",
                                                    tr("Surfaces (*.vtk *.gii )"));
    cout<<"not doing anything with file at the moment"<<endl;
}

void MainWindow::setCentreToPlaneIntersection()
{
    ui->openGLWidget->setCentreToPlaneIntersection();

}
void MainWindow::setCentreToSurfaceCentroid()
{
    ui->openGLWidget->setCentreToSurfaceCentroid();

}
void MainWindow::createPrimitive( const float & x, const float & y, const float & z)
{
    ui->openGLWidget->createSurfacePrimitive(glm::vec3(x,y,z));

}

void MainWindow::addSurface()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "/home",
                                                    tr("Surfaces (*.vtk *.gii )"));
    loadSurface(fileName);

    cout<<"not doing anything with file at the moment"<<endl;
}
void MainWindow::adjustImageLevel( int level ){
    ui->openGLWidget->setImageLevel( static_cast<float>(level)/ ui->sliderImageLevel->maximum() );

}

void MainWindow::adjustImageWindow( int window ){
    ui->openGLWidget->setImageWindow( static_cast<float>(window)/ ui->sliderImageWindow->maximum() );

}

void MainWindow::adjustImageOpacityThreshold( int opacity )
{
    ui->openGLWidget->setImageOpacityThreshold( static_cast<float>(opacity)/ ui->sliderImageOpacityThresh->maximum() );

}


void MainWindow::loadImage( const QString & filename )
{


    if (QFile::exists(filename))
        ui->openGLWidget->readImage(filename.toStdString());
    else
        cerr<<"Image file does not exist "<<filename.toStdString()<<endl;
    ui->sImX->setRange(0,ui->openGLWidget->imSizeX());
    ui->sImY->setRange(0,ui->openGLWidget->imSizeY());
    ui->sImZ->setRange(0,ui->openGLWidget->imSizeZ());

    ui->voxX->setRange(0,ui->openGLWidget->imSizeX());
    ui->voxY->setRange(0,ui->openGLWidget->imSizeY());
    ui->voxZ->setRange(0,ui->openGLWidget->imSizeZ());


    ui->sbX->setSingleStep(ui->openGLWidget->imDimX());
    ui->sbY->setSingleStep(ui->openGLWidget->imDimY());
    ui->sbZ->setSingleStep(ui->openGLWidget->imDimZ());



    //       Asr::Seg::vec3<float> origin = ui->openGLWidget->getImageOrigin();
    //       Asr::Seg::vec3<float> pixdim = ui->openGLWidget->getImagePixDim();
    //       array<float,9> R = ui->openGLWidget->getImageR();
    //       cout<<"pixdim "<<pixdim.x<<" "<<pixdim.y<<" "<<pixdim.z<<endl;
    qoffset_ = ui->openGLWidget->getImageOrigin();
    pixdims_ = ui->openGLWidget->getImagePixDim();
    R_ = ui->openGLWidget->getImageR();
    qfac_ = ui->openGLWidget->getImageQfac();
    cout<<"qfac "<<qfac_<<endl;
    Asr::Seg::vec3<float> min = getWorldCoord(0,0,0);
    Asr::Seg::vec3<float> max = getWorldCoord(ui->openGLWidget->imSizeX(),ui->openGLWidget->imSizeY(),ui->openGLWidget->imSizeZ());

    setWorldCoord();

    ui->sbX->setRange((min.x<max.x) ? min.x : max.x, (min.x<max.x) ? max.x: min.x );
    ui->sbY->setRange((min.y<max.y) ? min.y : max.y, (min.y<max.y) ? max.y: min.y );
    ui->sbZ->setRange((min.z<max.z) ? min.z : max.z, (min.z<max.z) ? max.z: min.z );



    ui->origin_x->setValue(qoffset_.x);
    ui->origin_y->setValue(qoffset_.y);
    ui->origin_z->setValue(qoffset_.z);


}
void MainWindow::loadSurface( const QString & filename )
{
    cout<<"LoadSurface "<<filename.toStdString()<<endl;
    ui->openGLWidget->readSurface(filename.toStdString());
    ui->openGLWidget->setCentreToSurfaceCentroid();

}

void MainWindow::addImage()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "/home",
                                                    tr("Image (*.nii *.nii.gz )"));

    if (QFile::exists(fileName))
        ui->openGLWidget->readImage(fileName.toStdString());

    ui->sImX->setRange(0,ui->openGLWidget->imSizeX());
    ui->sImY->setRange(0,ui->openGLWidget->imSizeY());
    ui->sImZ->setRange(0,ui->openGLWidget->imSizeZ());

}

void MainWindow::setLookVector(){
    ui->openGLWidget->setLookVector( ui->l_qa->text().toFloat(), ui->l_qb->text().toFloat(), ui->l_qc->text().toFloat(), ui->l_qd->text().toFloat() );

}

void MainWindow::setCameraCentre( const float & x, const float & y , const float & z )
{
    QString sx, sy,sz;
    sx.setNum(x);
    sy.setNum(y);
    sz.setNum(z);

    ui->l_cx->setText(sx);
    ui->l_cy->setText(sy);
    ui->l_cz->setText(sz);
    ui->openGLWidget->setCameraCentre( x,y,z );

}

void MainWindow::setLookVector( const float & a, const float & b, const float & c, const float & d ){


    QString sa, sb,sc,sd;
    sa.setNum(a);
    sb.setNum(b);
    sc.setNum(c);
    sd.setNum(d);
    ui->l_qa->setText(sa);
    ui->l_qb->setText(sb);
    ui->l_qc->setText(sc);
    ui->l_qd->setText(sd);
    ui->openGLWidget->setLookVector(a,b,c,d);



}

void  MainWindow::setCameraCentre(){
    cout<<"setcamera centre "<<endl;
    ui->openGLWidget->setCameraCentre( ui->l_cx->text().toFloat(), ui->l_cy->text().toFloat(), ui->l_cz->text().toFloat() );
}


Asr::Seg::vec3<float> MainWindow::getWorldCoord(const int & x, const int & y , const int & z )
{
    return Asr::Seg::vec3<float>(R_[0]* pixdims_.x*x + R_[1] * pixdims_.y*y + R_[2]*pixdims_.z*z + qoffset_.x,
            R_[3]* pixdims_.x*x + R_[4] * pixdims_.y*y + R_[5]*pixdims_.z*z + qoffset_.y,
            R_[6]* pixdims_.x*x + R_[7] * pixdims_.y*y + R_[8]*qfac_*pixdims_.z*z + qoffset_.z);
};
void  MainWindow::setWorldCoord( const Asr::Seg::vec3<float> & v_coord )
{
    ui->sbX->setValue(v_coord.x);
    ui->sbY->setValue(v_coord.y);
    ui->sbZ->setValue(v_coord.z);


}


void  MainWindow::setWorldCoord(  )
{
    setWorldCoord(getWorldCoord(ui->sbX->value(), ui->sbY->value(), ui->sbZ->value()));

}

void MainWindow::moveYZplane( int x)
{
    ui->sbX->setValue( R_[0]* pixdims_.x*x + R_[1] * pixdims_.y*ui->sbY->value() + R_[2]*pixdims_.z*ui->sbZ->value() + qoffset_.x);
    ui->voxX->setValue(x);
    //    ui->sbX->setValue(x * ui->openGLWidget->imDimX());
    ui->openGLWidget->setCurrentX(x);
}

void MainWindow::moveXZplane( int y)
{
    ui->sbY->setValue( R_[3]* pixdims_.x*ui->sbX->value() + R_[4] * pixdims_.y*y + R_[5]*pixdims_.z*ui->sbZ->value() + qoffset_.y);
    ui->voxY->setValue(y);

    //    ui->sbY->setValue(y * ui->openGLWidget->imDimY());

    ui->openGLWidget->setCurrentY(y);

}

void MainWindow::moveXYplane( int z){
    ui->sbZ->setValue( R_[6]* pixdims_.x*ui->sbX->value() + R_[7] * pixdims_.y*ui->sbY->value() + R_[8]*qfac_*pixdims_.z*z + qoffset_.z);
    ui->voxZ->setValue(z);

    //    ui->sbZ->setValue(z * ui->openGLWidget->imDimZ());

    ui->openGLWidget->setCurrentZ(z);

}

